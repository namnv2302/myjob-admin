import { Fragment, Suspense, useEffect } from "react";
import { ConfigProvider } from "antd";
import en_US from "antd/locale/en_US";
import { Routes, Route, useNavigate, useLocation } from "react-router-dom";
import { v4 as uuIdV4 } from "uuid";
import { publicRoutes } from "@constants/routers";
import MainLayout from "@layouts/MainLayout";
import { getAccessToken } from "@utils/localstorage";
import { whoAmI } from "@apis/auth";
import { useAppDispatch } from "redux/hooks";
import {
  AdminRole,
  login,
  logout,
} from "@slices/authorization/authorizationSlice";
import ProtectRoute from "@templates/ProtectRoute";
import { ROUTE_PATH } from "@constants/routes";

const App = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  useEffect(() => {
    (async () => {
      if (getAccessToken() == null && pathname === ROUTE_PATH.HOME) {
        return;
      }
      if (
        getAccessToken() == null &&
        pathname === ROUTE_PATH.EMPLOYER_SIGN_UP
      ) {
        return;
      }
      if (getAccessToken() == null) {
        return navigate(ROUTE_PATH.EMPLOYER_LOGIN);
      }
      const resAuth = await whoAmI();
      if (
        resAuth.status === 200 &&
        resAuth.data &&
        AdminRole.includes(resAuth.data.role)
      ) {
        dispatch(login(resAuth.data));
      } else {
        dispatch(logout());
      }
    })();
  }, [dispatch, navigate, pathname]);

  return (
    <ConfigProvider
      locale={en_US}
      prefixCls="mj"
      theme={{
        token: {
          fontFamily: "Inter",
          fontSize: 16,
          fontWeightStrong: 500,
          colorPrimary: "#007456",
        },
      }}
    >
      <div className="App">
        <Suspense fallback={<div>Loading</div>}>
          <Routes>
            {publicRoutes.map(({ path, component, layout }) => {
              const Page = component;
              let Layout = MainLayout as any;
              if (layout != null) {
                Layout = layout;
              } else if (layout === null) {
                Layout = Fragment;
              }

              return (
                <Route
                  key={uuIdV4()}
                  path={path}
                  element={
                    <Layout>
                      <ProtectRoute>
                        <Page />
                      </ProtectRoute>
                    </Layout>
                  }
                />
              );
            })}
          </Routes>
        </Suspense>
      </div>
    </ConfigProvider>
  );
};

export default App;
