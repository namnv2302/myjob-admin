const images = {
  logoImage: require("./logo.png"),
  lineYIcon: require("./line-xl.svg").default,
  freelancerIcon: require("./freelancer-1.svg").default,
  backgroundHeader: require("./background_header.png"),
  avatarDefault: require("./avatar-default.jpg"),
};

export default images;
