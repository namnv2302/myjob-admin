import { useEffect, useState } from "react";
import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import { getEmployerList } from "@apis/users";

const useEmployer = (current: number) => {
  const [data, setData] = useState<AuthorizationData[]>();
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getEmployerList(current);
      if (resp.status === 200) {
        setData(resp.data.data);
        setCurrentPage(resp.data.meta.current);
        setTotalPages(resp.data.meta.pages);
        setLoading(false);
      }
    })();
  }, [current]);

  return { data, currentPage, totalPages, loading };
};

export default useEmployer;
