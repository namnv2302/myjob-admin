import { useEffect, useState } from "react";
import { IPost } from "@slices/authorization/authorizationSlice";
import { getPostList } from "@apis/posts";

const usePosts = (current: number, limit: number) => {
  const [data, setData] = useState<IPost[]>();
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getPostList(current, limit);
      if (resp.status === 200) {
        setData(resp.data.data);
        setCurrentPage(resp.data.meta.current);
        setTotalPages(resp.data.meta.pages);
        setLoading(false);
      }
    })();
  }, [current, limit]);

  return { data, currentPage, totalPages, loading };
};

export default usePosts;
