import { useEffect, useState } from "react";
import { IResumes } from "@slices/authorization/authorizationSlice";
import { getResumesList } from "@apis/resumes";

const useResumes = (current: number, companyId?: number) => {
  const [data, setData] = useState<IResumes[]>();
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPages, setTotalPages] = useState<number>(1);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const resp = await getResumesList(current, companyId);
      if (resp.status === 200) {
        setData(resp.data.data);
        setCurrentPage(resp.data.meta.current);
        setTotalPages(resp.data.meta.pages);
        setLoading(false);
      }
    })();
  }, [current, companyId]);

  return { data, currentPage, totalPages, loading };
};

export default useResumes;
