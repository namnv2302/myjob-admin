import { useEffect, useState } from "react";
import { IChats } from "@slices/authorization/authorizationSlice";
import { getUserChats } from "@apis/chats";

const useChats = (id: string | undefined) => {
  const [data, setData] = useState<IChats[]>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      if (!id) return [];
      const resp = await getUserChats(id);
      if (resp.status === 200) {
        setData(resp.data);
      }
      setLoading(false);
    })();
  }, [id]);

  return { data, loading };
};

export default useChats;
