import { Typography } from "antd";
import classNames from "classnames/bind";
import styles from "./LoginLayout.module.scss";

const cx = classNames.bind(styles);

const LoginLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className={cx("wrapper")}>
      <div className={cx("inner")}>
        <div className={cx("container")}>{children}</div>
      </div>
      <Typography.Text className={cx("copyright")}>
        © 2024 NVN. All Right Reserved.
      </Typography.Text>
    </div>
  );
};

export default LoginLayout;
