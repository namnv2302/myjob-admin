import {
  ExclamationCircleOutlined,
  FormOutlined,
  GlobalOutlined,
  LogoutOutlined,
  MessageOutlined,
  MonitorOutlined,
  ProfileOutlined,
  TableOutlined,
  UploadOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { useCallback } from "react";
import { Modal, Image, Dropdown, Typography } from "antd";
import { Link, useNavigate } from "react-router-dom";
import ProLayout from "@ant-design/pro-layout";
import classNames from "classnames/bind";
import styles from "./AdminLayout.module.scss";
import { ADMIN_MENU, ROUTE_PATH } from "@constants/routes";
import images from "@assets/images";
import { clearAuthorizationToken } from "@utils/localstorage";
import { logout } from "@slices/authorization/authorizationSlice";
import { useAppDispatch, useAppSelector } from "redux/hooks";
import { AvatarDefaultIcon } from "@assets/icons";
import { Role } from "@constants/common";

const cx = classNames.bind(styles);

const IconMap: { [key: string]: React.ReactNode } = {
  dashboard: <TableOutlined />,
  users: <UserOutlined />,
  companies: <GlobalOutlined />,
  jobs: <MonitorOutlined />,
  cv: <UploadOutlined />,
  posts: <FormOutlined />,
  chat: <MessageOutlined />,
};

const menuHeaderRender = (
  logo: React.ReactNode,
  title: React.ReactNode,
  props?: any
) => (
  <Link to={ROUTE_PATH.HOME}>
    {logo}
    {!props?.collapsed && title}
  </Link>
);

const menuItemRender = (options: any, element: React.ReactNode) => {
  return <Link to={options.path}>{element}</Link>;
};

const AdminLayout = ({ children }: { children: React.ReactNode }) => {
  const authorization = useAppSelector((state) => state.authorization);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [modal, contextHolder] = Modal.useModal();

  const handleLogout = useCallback(() => {
    dispatch(logout());
    clearAuthorizationToken();
    navigate(`${ROUTE_PATH.EMPLOYER_LOGIN}`);
  }, [dispatch, navigate]);

  const loopMenuItem = useCallback(
    (menus: any): any =>
      menus.map(({ icon, routes, ...item }: any) => {
        if (authorization?.role !== Role[2].key && item.onlyAdmin) {
          // eslint-disable-next-line array-callback-return
          return;
        }
        return {
          ...item,
          icon: icon && IconMap[icon as string],
          children: routes && loopMenuItem(routes),
        };
      }),
    [authorization?.role]
  );

  const confirm = () => {
    modal.confirm({
      title: "Bạn có chắc chắn muốn đăng xuất?",
      icon: <ExclamationCircleOutlined />,
      okText: "Đăng xuất",
      cancelText: "Hủy",
      onOk: handleLogout,
    });
  };

  return (
    <>
      <ProLayout
        style={{ minHeight: "100vh" }}
        title="MyJob"
        fixSiderbar
        menuHeaderRender={menuHeaderRender}
        menuItemRender={menuItemRender}
        menuDataRender={() => loopMenuItem(ADMIN_MENU)}
        logo={
          <Image src={images.logoImage} preview={false} alt="Logo" width={22} />
        }
        avatarProps={{
          src: authorization?.avatar ? (
            <Image
              src={authorization?.avatar}
              preview={false}
              width={28}
              height={28}
              style={{ borderRadius: "100%", objectFit: "cover" }}
              alt="Avatar"
            />
          ) : (
            <AvatarDefaultIcon className={cx("avatar")} />
          ),
          size: "default",
          title: <Typography.Text>{authorization?.fullname}</Typography.Text>,
          render: (props, dom) => {
            return (
              <Dropdown
                menu={{
                  items: [
                    {
                      key: "profile",
                      icon: <ProfileOutlined />,
                      label: "Thông tin cá nhân",
                      onClick: () => navigate(ROUTE_PATH.EMPLOYER_PROFILE),
                    },
                    {
                      key: "logout",
                      icon: <LogoutOutlined />,
                      label: "Đăng xuất",
                      onClick: () => confirm(),
                    },
                  ],
                }}
              >
                {dom}
              </Dropdown>
            );
          },
        }}
      >
        {children}
      </ProLayout>
      {contextHolder}
    </>
  );
};

export default AdminLayout;
