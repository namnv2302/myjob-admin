import { useMemo } from "react";
import type { MenuProps } from "antd";
import { Menu, Typography } from "antd";
import classNames from "classnames/bind";
import styles from "@layouts/components/Header/Header.module.scss";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const NavigationBar = () => {
  const items: MenuProps["items"] = useMemo(() => {
    return [
      {
        label: (
          <Typography.Link href={ROUTE_PATH.HOME} className="text-default">
            Giới thiệu
          </Typography.Link>
        ),
        key: 1,
      },
      {
        label: (
          <Typography.Link href="#services" className="text-default">
            Dịch vụ
          </Typography.Link>
        ),
        key: 2,
      },
      {
        label: (
          <Typography.Link href="#services" className="text-default">
            Blog tuyển dụng
          </Typography.Link>
        ),
        key: 3,
      },
    ];
  }, []);

  return (
    <Menu
      className={cx("navigation-bar")}
      defaultSelectedKeys={["1"]}
      style={{ flex: 1 }}
      items={items}
      mode="horizontal"
    />
  );
};

export default NavigationBar;
