import { useCallback } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Button, Typography, Image, Space, Dropdown, Modal } from "antd";
import {
  CustomerServiceOutlined,
  ExclamationCircleOutlined,
  FormOutlined,
  HomeOutlined,
  LogoutOutlined,
  MenuOutlined,
  TableOutlined,
} from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./Header.module.scss";
import { ROUTE_PATH } from "@constants/routes";
import images from "@assets/images";
import NavigationBar from "@layouts/components/Header/components/NavigationBar";
import { useAppDispatch, useAppSelector } from "redux/hooks";
import { clearAuthorizationToken } from "@utils/localstorage";
import { logout } from "@slices/authorization/authorizationSlice";

const cx = classNames.bind(styles);

const Header = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [modal, contextHolder] = Modal.useModal();
  const authorization = useAppSelector((state) => state.authorization);

  const handleLogout = useCallback(() => {
    dispatch(logout());
    clearAuthorizationToken();
    navigate(`${ROUTE_PATH.EMPLOYER_LOGIN}`);
  }, [dispatch, navigate]);

  const confirm = () => {
    modal.confirm({
      title: "Bạn có chắc chắn muốn đăng xuất?",
      icon: <ExclamationCircleOutlined />,
      okText: "Đăng xuất",
      cancelText: "Hủy",
      onOk: handleLogout,
    });
  };

  return (
    <>
      <div className={cx("wrapper")}>
        <div className={cx("container")}>
          <div className={cx("left")}>
            <Link to={ROUTE_PATH.HOME} className={cx("logo")}>
              <Image
                src={images.logoImage}
                alt="Logo"
                width={46}
                className={cx("image")}
                preview={false}
              />
              <Typography.Title level={2} className={cx("logo-text")}>
                MyJob
              </Typography.Title>
            </Link>
            <Dropdown
              placement="topLeft"
              menu={{
                items: [
                  {
                    key: 1,
                    icon: <HomeOutlined />,
                    label: (
                      <Typography.Link
                        href={ROUTE_PATH.HOME}
                        className="text-default"
                      >
                        Giới thiệu
                      </Typography.Link>
                    ),
                  },
                  {
                    key: 2,
                    icon: <CustomerServiceOutlined />,
                    label: (
                      <Typography.Link
                        href="#services"
                        className="text-default"
                      >
                        Dịch vụ
                      </Typography.Link>
                    ),
                  },
                  {
                    key: 3,
                    icon: <FormOutlined />,
                    label: (
                      <Typography.Link
                        href="#services"
                        className="text-default"
                      >
                        Blog tuyển dụng
                      </Typography.Link>
                    ),
                  },
                ],
              }}
            >
              <MenuOutlined className={cx("mobile-menu_bar")} />
            </Dropdown>
            <NavigationBar />
          </div>
          <div className={cx("right")}>
            {authorization ? (
              <Dropdown
                placement="topRight"
                menu={{
                  items: [
                    {
                      key: "dashboard",
                      icon: <TableOutlined />,
                      label: "Trang quản trị",
                      onClick: () => navigate(ROUTE_PATH.DASHBOARD),
                    },
                    {
                      key: "logout",
                      icon: <LogoutOutlined />,
                      label: "Đăng xuất",
                      onClick: () => confirm(),
                    },
                  ],
                }}
              >
                <Space align="center" style={{ cursor: "pointer" }}>
                  <Image
                    src={authorization.avatar || images.avatarDefault}
                    width={28}
                    height={28}
                    preview={false}
                    style={{ borderRadius: "100%" }}
                    alt="avatar"
                  />
                  <Typography.Text>{authorization.fullname}</Typography.Text>
                </Space>
              </Dropdown>
            ) : (
              <>
                <Button
                  className={cx("button")}
                  // size="large"
                  type="primary"
                  ghost
                  onClick={() => navigate(ROUTE_PATH.EMPLOYER_LOGIN)}
                >
                  Đăng nhập
                </Button>
                <Button
                  className={cx("button")}
                  // size="large"
                  type="primary"
                  onClick={() => navigate(ROUTE_PATH.EMPLOYER_SIGN_UP)}
                >
                  Đăng ký
                </Button>
              </>
            )}
          </div>
        </div>
      </div>
      {contextHolder}
    </>
  );
};

export default Header;
