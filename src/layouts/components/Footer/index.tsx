import { Typography } from "antd";
import classNames from "classnames/bind";
import styles from "./Footer.module.scss";

const cx = classNames.bind(styles);

const Footer = () => {
  return (
    <div className={cx("wrapper")}>
      <div className={cx("body")}>
        <Typography.Text>© 2024 NVN. All Right Reserved.</Typography.Text>
      </div>
    </div>
  );
};

export default Footer;
