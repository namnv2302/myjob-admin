import { Navigate } from "react-router-dom";
import { ROUTE_PATH } from "@constants/routes";

import AdminLayout from "@layouts/AdminLayout";
import MainLayout from "@layouts/MainLayout";
import LoginLayout from "@layouts/LoginLayout";

//pages
import HomePage from "@pages/Home";
import LoginPage from "@pages/Login";
import SignUpPage from "@pages/SignUp";
import NotFoundPage from "@pages/NotFound";
import CompaniesPage from "@pages/Companies";
import DashboardPage from "@pages/Dashboard";
import UsersNormalPage from "@pages/UsersNormal";
import EmployerPage from "@pages/Employer";
import ProfilePage from "@pages/Profile";
import JobsPage from "@pages/Jobs";
import JobsCreatePage from "@pages/JobsCreate";
import JobsDetailPage from "@pages/JobsDetail";
import CVPage from "@pages/CV";
import PostsPage from "@pages/Posts";
import ChatsPage from "@pages/Chats";

type publicRoutesType = {
  path: string;
  component: React.FC;
  layout?: null | any;
};

export const NavigateToNotFound = () => {
  return <Navigate to={ROUTE_PATH.NOT_FOUND} />;
};

export const NavigateToSignIn = () => {
  return <Navigate to={ROUTE_PATH.EMPLOYER_LOGIN} />;
};

export const publicRoutes: publicRoutesType[] = [
  {
    path: ROUTE_PATH.EMPLOYER_LOGIN,
    component: LoginPage,
    layout: LoginLayout,
  },
  {
    path: ROUTE_PATH.EMPLOYER_SIGN_UP,
    component: SignUpPage,
    layout: LoginLayout,
  },
  {
    path: ROUTE_PATH.HOME,
    component: HomePage,
    layout: MainLayout,
  },
  {
    path: ROUTE_PATH.COMPANIES,
    component: CompaniesPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.DASHBOARD,
    component: DashboardPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.USERS_NORMAL,
    component: UsersNormalPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.USERS_EMPLOYER,
    component: EmployerPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.EMPLOYER_PROFILE,
    component: ProfilePage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.JOBS,
    component: JobsPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.JOBS_CREATE,
    component: JobsCreatePage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.JOBS_DETAIL,
    component: JobsDetailPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.CV,
    component: CVPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.POSTS,
    component: PostsPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.CHATS,
    component: ChatsPage,
    layout: AdminLayout,
  },
  {
    path: ROUTE_PATH.NOT_FOUND,
    component: NotFoundPage,
    layout: null,
  },
  {
    path: ROUTE_PATH.OTHER,
    component: NotFoundPage,
    layout: null,
  },
];
