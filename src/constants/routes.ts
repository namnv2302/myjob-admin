export const ROUTE_PATH = {
  HOME: "/",
  EMPLOYER_LOGIN: "/employer/login",
  EMPLOYER_SIGN_UP: "/employer/sign-up",
  EMPLOYER_PROFILE: "/employer/profile",
  DASHBOARD: "/dashboard",
  COMPANIES: "/companies",
  USERS_NORMAL: "/users/normal",
  USERS_EMPLOYER: "/users/employer",
  JOBS: "/jobs",
  JOBS_CREATE: "/jobs/create",
  JOBS_DETAIL: "/jobs/:id",
  CV: "/cv",
  POSTS: "/posts",
  CHATS: "/chats",
  NOT_FOUND: "404",
  OTHER: "*",
};

export const ADMIN_MENU = [
  {
    path: "/dashboard",
    name: "Thống kê",
    icon: "dashboard",
  },
  {
    name: "Người dùng",
    icon: "users",
    onlyAdmin: true,
    routes: [
      {
        path: "/users/normal",
        name: "Ứng viên",
      },
      {
        path: "/users/employer",
        name: "Nhà tuyển dụng",
      },
    ],
  },
  {
    path: "/companies",
    name: "Công ty",
    icon: "companies",
  },
  {
    path: "/jobs",
    name: "Việc làm",
    icon: "jobs",
  },
  {
    path: "/cv",
    name: "Hồ sơ(CV)",
    icon: "cv",
  },
  {
    path: "/posts",
    name: "Bài viết",
    icon: "posts",
    onlyAdmin: true,
  },
  {
    path: "/chats",
    name: "Tin nhắn",
    icon: "chat",
  },
];
