export const Role = [
  {
    key: "user",
    label: "Ứng viên",
  },
  {
    key: "employer",
    label: "Nhà tuyển dụng",
  },
  {
    key: "admin",
    label: "Quản trị viên",
  },
];

export const Gender = [
  {
    key: "male",
    label: "Nam",
  },
  {
    key: "female",
    label: "Nữ",
  },
];

export const Skills = [
  "Frontend",
  "Backend",
  "Nestjs",
  "Reactjs",
  "PHP",
  "Vuejs",
  "Python",
  "Nodejs/Express",
];

export const Level = [
  {
    key: 0,
    label: "Không yêu cầu",
  },
  {
    key: 1,
    label: "Fresher",
  },
  {
    key: 2,
    label: "Junior",
  },
  {
    key: 3,
    label: "Middle",
  },
  {
    key: 4,
    label: "Senior",
  },
  {
    key: 5,
    label: "TechLead",
  },
];

export const Status = [
  {
    key: 0,
    label: "Chờ xử lý",
  },
  {
    key: 1,
    label: "Đồng ý",
  },
  {
    key: 2,
    label: "Từ chối",
  },
];

export const phoneExp = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
