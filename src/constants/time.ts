export const DEFAULT_TIME_FORMAT = "YYYY/MM/DD hh:mm";

export const DEFAULT_TIME_ONLY_FORMAT = "hh:mm:ss";

export const DEFAULT_DATE_FORMAT = "YYYY/MM/DD";
