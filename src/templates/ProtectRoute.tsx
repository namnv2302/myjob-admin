import { useAppSelector } from "redux/hooks";
import { NavigateToSignIn } from "@constants/routers";

const ProtectRoute = ({ children }: { children: any }) => {
  const authorization = useAppSelector((state) => state.authorization);

  if (!authorization) {
    <NavigateToSignIn />;
  }

  return children;
};

export default ProtectRoute;
