import axiosInstance from "@utils/axios";
import { ICompanies } from "@slices/authorization/authorizationSlice";

enum CompaniesPath {
  Default = "/companies",
  Update = "/companies/:id",
  Delete = "/companies/:id",
}

export const getCompaniesList = async (current: number = 1) => {
  return await axiosInstance.get(CompaniesPath.Default, {
    params: { current },
  });
};

export const createCompany = async (payload: ICompanies) => {
  return await axiosInstance.post(CompaniesPath.Default, { ...payload });
};

export const updateCompany = async (id: string, payload: ICompanies) => {
  return await axiosInstance.patch(CompaniesPath.Update.replace(":id", id), {
    ...payload,
  });
};

export const deleteCompany = async (id: string) => {
  return await axiosInstance.delete(CompaniesPath.Delete.replace(":id", id));
};
