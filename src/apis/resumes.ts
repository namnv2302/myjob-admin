import axiosInstance from "@utils/axios";

enum ResumesPath {
  Default = "/resumes",
  Detail = "/resumes/:id",
  InfoAll = "/resumes/info-all",
}

export const getResumesList = async (
  current: number = 1,
  companyId?: number
) => {
  return await axiosInstance.get(ResumesPath.Default, {
    params: { current, companyId },
  });
};

export const updateResumes = async (
  id: string,
  { status }: { status: number }
) => {
  return await axiosInstance.patch(ResumesPath.Detail.replace(":id", id), {
    status,
  });
};

export const getInfoAll = async (companyId: number) => {
  return await axiosInstance.get(ResumesPath.InfoAll, {
    params: { companyId },
  });
};
