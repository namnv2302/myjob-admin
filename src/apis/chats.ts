import axiosInstance from "@utils/axios";

export enum ChatsPath {
  GetUserChats = "/chats/:userId/users",
}

export const getUserChats = async (userId: string) => {
  return await axiosInstance.get(
    ChatsPath.GetUserChats.replace(":userId", userId)
  );
};
