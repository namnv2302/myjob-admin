import axiosInstance from "@utils/axios";

enum UploadPath {
  Default = "/files/upload",
}

export const upload = async (file: any, folderName?: string) => {
  return await axiosInstance.post(
    UploadPath.Default,
    { file: file, folderName: folderName },
    {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    }
  );
};
