// import { IJobs } from "@slices/authorization/authorizationSlice";
import axiosInstance from "@utils/axios";
import { FormDataJobType } from "@pages/JobsCreate/components/CreateCard";

enum JobsPath {
  Default = "/jobs",
  Detail = "/jobs/:id",
}

export const createJob = async (payload: FormDataJobType) => {
  return await axiosInstance.post(JobsPath.Default, { ...payload });
};

export const getJobsList = async (current: number = 1, companyId?: number) => {
  return await axiosInstance.get(`${JobsPath.Default}/admin`, {
    params: { current, companyId },
  });
};

export const getJobDetail = async (id: string) => {
  return await axiosInstance.get(JobsPath.Detail.replace(":id", id));
};

export const updateJob = async (id: string, payload: FormDataJobType) => {
  return await axiosInstance.patch(JobsPath.Detail.replace(":id", id), {
    ...payload,
  });
};

export const deleteJob = async (id: string) => {
  return await axiosInstance.delete(JobsPath.Detail.replace(":id", id));
};
