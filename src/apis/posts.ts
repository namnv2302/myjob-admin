import axiosInstance from "@utils/axios";

enum PostsPath {
  Default = "/posts",
  Detail = "/posts/:id",
}

export const getPostList = async (current: number = 1, limit: number = 10) => {
  return await axiosInstance.get(PostsPath.Default, {
    params: { current, limit },
  });
};

export const deletePost = async (id: string) => {
  return await axiosInstance.delete(PostsPath.Detail.replace(":id", id));
};
