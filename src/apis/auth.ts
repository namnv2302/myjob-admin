import axiosInstance from "@utils/axios";

enum AuthPath {
  Login = "/auth/login",
  SignUpEmployer = "/auth/sign-up/employer",
  Me = "/auth/me",
}

export const login = async (email: string, password: string) => {
  return await axiosInstance.post(AuthPath.Login, {
    username: email,
    password,
  });
};

export const whoAmI = async () => {
  return await axiosInstance.get(AuthPath.Me);
};

export const signUpEmployer = async (payload: any) => {
  return await axiosInstance.post(AuthPath.SignUpEmployer, { ...payload });
};
