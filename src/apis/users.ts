import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import axiosInstance from "@utils/axios";

enum UsersPath {
  Default = "/users",
  Detail = "/users/:id",
  UsersNormal = "/users/normal",
  Employer = "/users/employer",
  UpdateByAdmin = "/users/:id/admin",
  UpdateEmployer = "/users/:id/employer",
  Delete = "/users/:id",
}

export const createNormalUser = async (data: AuthorizationData) => {
  return await axiosInstance.post(UsersPath.Default, { ...data });
};

export const createEmployer = async (data: AuthorizationData) => {
  return await axiosInstance.post(UsersPath.Employer, { ...data });
};

export const getUsersNormalList = async (current: number = 1) => {
  return await axiosInstance.get(UsersPath.UsersNormal, {
    params: { current },
  });
};

export const getEmployerList = async (current: number = 1) => {
  return await axiosInstance.get(UsersPath.Employer, {
    params: { current },
  });
};

export const update = async (id: string, data: AuthorizationData) => {
  return await axiosInstance.patch(UsersPath.Detail.replace(":id", id), {
    ...data,
  });
};

export const updateByAdmin = async (id: string, data: AuthorizationData) => {
  return await axiosInstance.patch(UsersPath.UpdateByAdmin.replace(":id", id), {
    ...data,
  });
};

export const updateEmployer = async (id: string, data: AuthorizationData) => {
  return await axiosInstance.patch(
    UsersPath.UpdateEmployer.replace(":id", id),
    { ...data }
  );
};

export const deleteUser = async (id: string) => {
  return await axiosInstance.delete(UsersPath.Delete.replace(":id", id));
};
