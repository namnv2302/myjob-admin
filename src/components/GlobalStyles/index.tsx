import "./GlobalStyles.scss";

const GlobalStyles = ({ children }: { children: React.ReactElement }) => {
  return children;
};

export default GlobalStyles;
