import { useNavigate } from "react-router-dom";
import { Button, Result } from "antd";
import type { ResultStatusType } from "antd/es/result";

const NotPermission = ({
  status,
  title,
  subTitle,
}: {
  status: ResultStatusType;
  title: string;
  subTitle: string;
}) => {
  const navigate = useNavigate();

  return (
    <Result
      status={status}
      title={title}
      subTitle={subTitle}
      extra={
        <Button type="primary" onClick={() => navigate(-1)}>
          Quay lại
        </Button>
      }
    />
  );
};

export default NotPermission;
