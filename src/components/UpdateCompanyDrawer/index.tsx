import { memo, useCallback, useEffect, useMemo, useState } from "react";
import axios from "axios";
import {
  Button,
  Col,
  Drawer,
  Form,
  Input,
  Row,
  Select,
  Upload,
  message,
  Image,
  Empty,
} from "antd";
import ImgCrop from "antd-img-crop";
import { UploadOutlined } from "@ant-design/icons";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import {
  DistrictType,
  ProvinceType,
} from "@pages/SignUp/components/SignUpForm";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import { upload } from "@apis/upload";
import { updateCompany } from "@apis/companies";

const UpdateCompanyDrawer = ({
  company,
  open,
  onOpen,
}: {
  company: ICompanies | null;
  open: boolean;
  onOpen: any;
}) => {
  const [form] = Form.useForm();
  const companyData = useMemo(() => {
    if (company) return company;
  }, [company]);
  const [provinceList, setProvinceList] = useState<ProvinceType[]>([]);
  const [districtList, setDistrictList] = useState<DistrictType[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [previewLogo, setPreviewLogo] = useState<any>(null);
  const [text, setText] = useState<string>("");

  useEffect(() => {
    form.setFieldsValue({
      companyName: companyData?.companyName,
      scales: companyData?.scales,
      provinceName: companyData?.provinceName,
      districtName: companyData?.districtName,
    });
  }, [form, companyData]);

  useEffect(() => {
    if (companyData?.introduction) {
      setText(companyData.introduction);
    } else {
      setText("");
    }
  }, [companyData?.introduction]);

  useEffect(() => {
    (async () => {
      const resp = await axios.get(
        `${process.env.REACT_APP_PROVINCE_BASE_URL}/province`
      );
      if (resp.status === 200) {
        const list = resp.data.results.map((item: ProvinceType) => ({
          key: item.province_id,
          value: item.province_name,
          label: item.province_name,
        }));
        setProvinceList(list);
      }
    })();
  }, []);

  const handleCloseDrawer = useCallback(() => {
    onOpen(false);
    form.resetFields();
    if (previewLogo) {
      URL.revokeObjectURL(previewLogo.preview);
      setPreviewLogo(null);
    }
    if (companyData?.introduction) {
      setText(`${companyData?.introduction}`);
    }
  }, [form, onOpen, previewLogo, companyData]);

  const handleChooseProvince = useCallback(
    async (value: string, option: any) => {
      if (option) {
        const resp = await axios.get(
          `${process.env.REACT_APP_PROVINCE_BASE_URL}/province/district/${option.key}`
        );
        if (resp.status === 200) {
          const list = resp.data.results.map((item: DistrictType) => ({
            value: item.district_name,
            label: item.district_name,
          }));

          setDistrictList(list);
        }
      }
    },
    []
  );

  const handleBeforeUpload = useCallback(async (file: any) => {
    file.preview = URL.createObjectURL(file);
    setPreviewLogo(file);
  }, []);

  const handleFinish = useCallback(
    async (data: ICompanies) => {
      setLoading(true);
      try {
        if (previewLogo) {
          const resp = await upload(previewLogo, "companies");
          if (resp.status === 201 && resp.data) {
            const respCompany = await updateCompany(`${companyData?.id}`, {
              ...data,
              introduction: text.trim(),
              logo: resp.data.secure_url,
            });
            if (respCompany.status === 200) {
              message.success("Cập nhật thành công");
            }
          } else {
            message.error("Tải ảnh có lỗi. Thử lại sau!");
          }
        } else {
          const respCompany = await updateCompany(`${companyData?.id}`, {
            ...data,
            introduction: text.trim(),
          });
          if (respCompany.status === 200) {
            message.success("Cập nhật thành công");
          }
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error("Có lỗi. Thử lại sau!");
        }
      }
      setLoading(false);
      handleCloseDrawer();
    },
    [companyData?.id, previewLogo, handleCloseDrawer, text]
  );

  return (
    <Drawer
      title="Chỉnh sửa thông tin công ty"
      placement="right"
      width={500}
      open={open}
      forceRender
      onClose={handleCloseDrawer}
    >
      {companyData ? (
        <Form
          form={form}
          layout="vertical"
          requiredMark="optional"
          initialValues={{
            companyName: companyData?.companyName,
            scales: companyData?.scales,
            provinceName: companyData?.provinceName,
            districtName: companyData?.districtName,
          }}
          // fields={[
          //   {
          //     name: ["companyName"],
          //     value: companyData?.companyName,
          //   },
          //   {
          //     name: ["scales"],
          //     value: companyData?.scales,
          //   },
          //   {
          //     name: ["provinceName"],
          //     value: companyData?.provinceName,
          //   },
          //   {
          //     name: ["districtName"],
          //     value: companyData?.districtName,
          //   },
          // ]}
          onFinish={handleFinish}
        >
          <Form.Item
            label="Công ty"
            name="companyName"
            rules={[{ required: true, message: "Vui lòng nhập công ty" }]}
          >
            <Input placeholder="Công ty" />
          </Form.Item>
          <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
            <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <Form.Item
                name="provinceName"
                label="Địa điểm làm việc"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn địa điểm làm việc",
                  },
                ]}
              >
                <Select
                  placeholder="Chọn tỉnh/thành phố"
                  showSearch
                  allowClear
                  options={provinceList}
                  onChange={handleChooseProvince}
                />
              </Form.Item>
            </Col>
            <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <Form.Item
                name="districtName"
                label="Quận/huyện"
                rules={[
                  { required: true, message: "Vui lòng chọn quận/huyện" },
                ]}
              >
                <Select
                  placeholder="Chọn quận/huyện"
                  showSearch
                  allowClear
                  options={districtList}
                />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item label="Giới thiệu" required>
            <CKEditor
              editor={ClassicEditor}
              data={text}
              onChange={(event, editor) => {
                const data = editor.getData();
                setText(data);
              }}
            />
          </Form.Item>
          <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
            <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <Form.Item label="Logo" required>
                <ImgCrop rotationSlider quality={1} aspect={1}>
                  <Upload
                    accept="image/jpg, image/jpeg, image/png"
                    beforeUpload={handleBeforeUpload}
                  >
                    <Button icon={<UploadOutlined />} block>
                      Chọn ảnh
                    </Button>
                  </Upload>
                </ImgCrop>
              </Form.Item>
            </Col>
            <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <Form.Item label="Quy mô" name="scales" required>
                <Input placeholder="Quy mô" />
              </Form.Item>
            </Col>
          </Row>
          {previewLogo || companyData?.logo ? (
            <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
                <Form.Item required>
                  <Image
                    src={
                      (previewLogo && previewLogo.preview) || companyData?.logo
                    }
                    alt="Logo"
                    width={150}
                    height={150}
                    preview={false}
                  />
                </Form.Item>
              </Col>
            </Row>
          ) : (
            false
          )}
          <Form.Item style={{ marginBottom: 0 }}>
            <Button htmlType="submit" block type="primary" loading={loading}>
              Hoàn tất
            </Button>
          </Form.Item>
        </Form>
      ) : (
        <Empty
          image={Empty.PRESENTED_IMAGE_SIMPLE}
          description="Không có công ty"
        />
      )}
    </Drawer>
  );
};

export default memo(UpdateCompanyDrawer);
