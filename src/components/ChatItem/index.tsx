import { memo } from "react";
import { Image } from "antd";
import classNames from "classnames/bind";
import styles from "./ChatItem.module.scss";
import images from "@assets/images";

const cx = classNames.bind(styles);

const ChatItem = () => {
  return (
    <div className={cx("wrapper")}>
      <Image
        src={images.avatarDefault}
        alt=""
        width={40}
        height={40}
        preview={false}
        style={{ borderRadius: "100%" }}
      />
      <div className={cx("info")}>
        <p className={cx("name")}>Nguyễn Văn Nam</p>
        <span className={cx("message")}>ahahahjsjsjsjs</span>
      </div>
    </div>
  );
};

export default memo(ChatItem);
