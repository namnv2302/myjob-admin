import { memo, useCallback, useEffect, useMemo, useState } from "react";
import {
  DeleteOutlined,
  EditOutlined,
  EyeOutlined,
  MailOutlined,
  PhoneOutlined,
  PlusOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import { ProTable } from "@ant-design/pro-components";
import { Button, Pagination, Popconfirm, Tag, message } from "antd";
import {
  AuthorizationData,
  ICompanies,
} from "@slices/authorization/authorizationSlice";
import useEmployer from "@hooks/users/useEmployer";
import UpdateCompanyDrawer from "@components/UpdateCompanyDrawer";
import CreateModal from "@pages/Employer/components/CreateModal";
import UpdateDrawer from "@pages/Employer/components/UpdateDrawer";
import { deleteUser, getEmployerList } from "@apis/users";
import { Role } from "@constants/common";

const EmployerTable = () => {
  const { data, loading, currentPage, totalPages } = useEmployer(1);
  const [current, setCurrent] = useState<number>();
  const [employerList, setEmployerList] = useState<AuthorizationData[]>();
  const [openDrawerViewCompany, setOpenDrawerViewCompany] =
    useState<boolean>(false);
  const [companyData, setCompanyData] = useState<ICompanies | null>(null);
  const [openCreateModal, setOpenCreateModal] = useState<boolean>(false);
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);
  const [employerData, setEmployerData] = useState<AuthorizationData | null>(
    null
  );
  const [fetching, setFetching] = useState<boolean>(false);

  useEffect(() => {
    if (data) {
      setEmployerList(data);
      setCurrent(currentPage);
    }
  }, [data, currentPage]);

  const handleViewCompany = useCallback((record: any) => {
    setCompanyData(record.company);
    setOpenDrawerViewCompany(true);
  }, []);

  const handleBeforeOpenDrawer = useCallback((record: AuthorizationData) => {
    setOpenDrawer(true);
    setEmployerData(record);
  }, []);

  const handleDeleteEmployer = useCallback(async (id: string) => {
    try {
      await deleteUser(id);
      message.success("Xóa thành công, hãy cập nhật lại");
    } catch (error) {
      message.error("Có lỗi tạm thời không thể xóa");
    }
  }, []);

  const columns: any = useMemo(
    () => [
      {
        title: "Họ tên",
        key: "fullname",
        dataIndex: "fullname",
      },
      {
        title: "Địa chỉ email",
        key: "email",
        dataIndex: "email",
        render: (email: string) => (
          <p style={{ display: "flex", gap: "8px" }}>
            <MailOutlined />
            <span>{email}</span>
          </p>
        ),
      },
      {
        title: "Số điện thoại",
        key: "phoneNumber",
        dataIndex: "phoneNumber",
        render: (phoneNumber: string) => (
          <p style={{ display: "flex", gap: "8px" }}>
            <PhoneOutlined />
            <span>{phoneNumber}</span>
          </p>
        ),
      },
      {
        title: "Vai trò",
        key: "role",
        dataIndex: "role",
        render: (value: string) => {
          if (value === "admin") {
            return (
              <Tag color="success" className="tag">
                {value}
              </Tag>
            );
          }
          return (
            <Tag color="volcano" className="tag">
              {value}
            </Tag>
          );
        },
      },
      {
        title: "Công ty",
        key: "company",
        align: "center" as "center",
        render: (_: any, record: AuthorizationData) => (
          <>
            <Button
              type="primary"
              ghost
              icon={<EyeOutlined />}
              onClick={() => handleViewCompany(record)}
            ></Button>
          </>
        ),
      },
      {
        title: "Cập nhật",
        key: "action",
        align: "center" as "center",
        render: (_: any, record: AuthorizationData) => (
          <>
            <Button
              type="primary"
              ghost
              icon={<EditOutlined />}
              onClick={() => handleBeforeOpenDrawer(record)}
            ></Button>
            {record.role === Role[2].key ? (
              false
            ) : (
              <Popconfirm
                title="Xóa nhà tuyển dụng"
                description="Bạn có chắc chắn không?"
                okText="Xóa"
                cancelText="Hủy"
                onConfirm={() => handleDeleteEmployer(`${record.id}`)}
              >
                <Button
                  danger
                  icon={<DeleteOutlined color="red" />}
                  style={{ marginLeft: 8 }}
                ></Button>
              </Popconfirm>
            )}
          </>
        ),
      },
    ],
    [handleViewCompany, handleBeforeOpenDrawer, handleDeleteEmployer]
  );

  const handleChangePage = useCallback(async (page: number) => {
    setFetching(true);
    try {
      const resp = await getEmployerList(page);
      if (resp.status === 200) {
        setEmployerList(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, []);

  const handleReload = useCallback(async () => {
    setFetching(true);
    try {
      const resp = await getEmployerList();
      if (resp.status === 200) {
        setEmployerList(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, []);

  return (
    <>
      <ProTable
        rowKey="id"
        headerTitle="Danh sách nhà tuyển dụng"
        bordered
        cardBordered
        search={false}
        options={false}
        pagination={false}
        loading={loading || fetching}
        columns={columns}
        dataSource={employerList}
        toolBarRender={() => [
          <>
            <Button
              key="button-add"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => setOpenCreateModal(true)}
            >
              Thêm
            </Button>
            <Button
              key="button-reload"
              icon={<ReloadOutlined />}
              onClick={handleReload}
            >
              Tải lại
            </Button>
          </>,
        ]}
      />
      <Pagination
        style={{ marginTop: 26, textAlign: "center" }}
        current={current}
        pageSize={10}
        total={10 * totalPages}
        onChange={handleChangePage}
      />

      <UpdateCompanyDrawer
        company={companyData}
        open={openDrawerViewCompany}
        onOpen={setOpenDrawerViewCompany}
      />

      <CreateModal open={openCreateModal} onOpen={setOpenCreateModal} />

      <UpdateDrawer
        open={openDrawer}
        onOpen={setOpenDrawer}
        data={employerData}
      />
    </>
  );
};

export default memo(EmployerTable);
