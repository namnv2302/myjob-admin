import { useCallback, useState } from "react";
import axios from "axios";
import {
  Button,
  Col,
  Form,
  Image,
  Input,
  Modal,
  Row,
  Select,
  Upload,
  message,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import ImgCrop from "antd-img-crop";
import { Gender, Role, phoneExp } from "@constants/common";
import useCompanies from "@hooks/companies/useCompanies";
import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import { upload } from "@apis/upload";
import { createEmployer } from "@apis/users";

const CreateModal = ({ open, onOpen }: { open: boolean; onOpen: any }) => {
  const [form] = Form.useForm();
  const { data, loading } = useCompanies(1);
  const [previewAvatar, setPreviewAvatar] = useState<any>(null);
  const [creating, setCreating] = useState<boolean>(false);

  const handleBeforeUpload = useCallback(async (file: any) => {
    file.preview = URL.createObjectURL(file);
    setPreviewAvatar(file);
  }, []);

  const handleCloseModal = useCallback(() => {
    onOpen(false);
    form.resetFields();
    if (previewAvatar) {
      URL.revokeObjectURL(previewAvatar.preview);
      setPreviewAvatar(null);
    }
  }, [onOpen, previewAvatar, form]);

  const handleFinish = useCallback(
    async (dataForm: AuthorizationData) => {
      setCreating(true);
      try {
        if (previewAvatar) {
          const resp = await upload(previewAvatar, "employers");
          if (resp.status === 201 && resp.data) {
            const respEmployer = await createEmployer({
              ...dataForm,
              avatar: resp.data.secure_url,
            });
            if (respEmployer.status === 201) {
              message.success("Tạo thành công");
            }
          } else {
            message.error("Tải ảnh có lỗi. Thử lại sau!");
          }
        } else {
          const respEmployer = await createEmployer({ ...dataForm });
          if (respEmployer.status === 201) {
            message.success("Tạo thành công");
          }
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error("Có lỗi. Thử lại sau!");
        }
      }
      setCreating(false);
      handleCloseModal();
    },
    [previewAvatar, handleCloseModal]
  );

  return (
    <Modal
      title="Thêm nhà tuyển dụng"
      open={open}
      footer={false}
      onCancel={() => onOpen(false)}
    >
      <Form
        form={form}
        name="create-employer"
        layout="vertical"
        autoComplete="off"
        requiredMark="optional"
        onFinish={handleFinish}
      >
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="fullname"
              label="Họ tên"
              rules={[{ required: true, message: "Họ tên không được trống" }]}
            >
              <Input placeholder="Họ tên" />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="email"
              label="Email"
              rules={[
                { required: true, message: "Email không được trống" },
                { type: "email", message: "Không đúng định dạng email" },
              ]}
            >
              <Input placeholder="Email" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="password"
              label="Mật khẩu"
              rules={[{ required: true, message: "Mật khẩu không được trống" }]}
            >
              <Input.Password placeholder="Mật khẩu" />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="phoneNumber"
              label="Số điện thoại"
              required
              rules={[
                // { required: true, message: "Số điện thoại không được trống" },
                {
                  validator(_, value) {
                    if (phoneExp.test(value)) {
                      return Promise.resolve();
                    }
                    return Promise.reject("Số điện thoại phải đủ 10 số");
                  },
                },
              ]}
            >
              <Input placeholder="Số điện thoại" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="gender"
              label="Giới tính"
              rules={[
                { required: true, message: "Giới tính không được trống" },
              ]}
            >
              <Select
                placeholder="Chọn giới tính"
                options={Gender.map(({ key, label }) => ({
                  value: key,
                  label,
                }))}
              />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item label="Ảnh đại diện" required>
              <ImgCrop rotationSlider quality={1} aspect={1}>
                <Upload
                  accept="image/jpg, image/jpeg, image/png"
                  beforeUpload={handleBeforeUpload}
                >
                  <Button icon={<UploadOutlined />} block>
                    Chọn ảnh
                  </Button>
                </Upload>
              </ImgCrop>
            </Form.Item>
          </Col>
        </Row>
        {previewAvatar ? (
          <Row gutter={{ xl: 16 }}>
            <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
              <Form.Item required>
                <Image
                  src={previewAvatar.preview}
                  alt="Avatar"
                  width={130}
                  height={130}
                  preview={false}
                />
              </Form.Item>
            </Col>
          </Row>
        ) : (
          false
        )}
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="role"
              label="Vai trò"
              rules={[{ required: true, message: "Vai trò không được trống" }]}
            >
              <Select
                placeholder="Chọn vai trò"
                options={Role.map(({ key, label }) => {
                  if (key === "user") {
                    return {};
                  }

                  return {
                    value: key,
                    label,
                  };
                })}
              />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="company"
              label="Thuộc công ty"
              rules={[{ required: true, message: "Chọn công ty" }]}
            >
              <Select
                placeholder="Thuộc công ty"
                loading={loading}
                options={data?.map(({ id, companyName }) => ({
                  key: id,
                  value: id,
                  label: companyName,
                }))}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item style={{ marginBottom: 0 }}>
              <Button type="primary" block htmlType="submit" loading={creating}>
                Thêm
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default CreateModal;
