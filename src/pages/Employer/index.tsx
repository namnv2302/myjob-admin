import EmployerTable from "@pages/Employer/components/EmployerTable";
import { useAppSelector } from "redux/hooks";
import { Role } from "@constants/common";
import NotPermission from "@components/NotPermission";

const EmployerPage = () => {
  const authorization = useAppSelector((state) => state.authorization);

  return (
    <>
      {authorization?.role === Role[2].key ? (
        <EmployerTable />
      ) : (
        <NotPermission
          status="403"
          title="403"
          subTitle="Bạn không được phép truy cập trang này"
        />
      )}
    </>
  );
};

export default EmployerPage;
