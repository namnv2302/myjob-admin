import { memo, useCallback, useEffect, useState } from "react";
import axios from "axios";
import {
  Button,
  Col,
  Form,
  Image,
  Input,
  Modal,
  Row,
  Select,
  Upload,
  message,
} from "antd";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import ImgCrop from "antd-img-crop";
import { UploadOutlined } from "@ant-design/icons";
import {
  DistrictType,
  ProvinceType,
} from "@pages/SignUp/components/SignUpForm";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import { createCompany } from "@apis/companies";
import { upload } from "@apis/upload";

const CreateModal = ({ open, onOpen }: { open: boolean; onOpen: any }) => {
  const [form] = Form.useForm();
  const [provinceList, setProvinceList] = useState<ProvinceType[]>([]);
  const [districtList, setDistrictList] = useState<DistrictType[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [previewLogo, setPreviewLogo] = useState<any>(null);
  const [introduction, setIntroduction] = useState<string>("");

  // Get province list
  useEffect(() => {
    (async () => {
      const resp = await axios.get(
        `${process.env.REACT_APP_PROVINCE_BASE_URL}/province`
      );
      if (resp.status === 200) {
        const list = resp.data.results.map((item: ProvinceType) => ({
          key: item.province_id,
          value: item.province_name,
          label: item.province_name,
        }));
        setProvinceList(list);
      }
    })();
  }, []);

  const handleChooseProvince = useCallback(
    async (value: string, option: any) => {
      if (option) {
        const resp = await axios.get(
          `${process.env.REACT_APP_PROVINCE_BASE_URL}/province/district/${option.key}`
        );
        if (resp.status === 200) {
          const list = resp.data.results.map((item: DistrictType) => ({
            value: item.district_name,
            label: item.district_name,
          }));

          setDistrictList(list);
        }
      }
    },
    []
  );

  const handleBeforeUpload = useCallback(async (file: any) => {
    file.preview = URL.createObjectURL(file);
    setPreviewLogo(file);
  }, []);

  const handleCloseModal = useCallback(() => {
    onOpen(false);
    form.resetFields();
    if (previewLogo) {
      URL.revokeObjectURL(previewLogo.preview);
      setPreviewLogo(null);
    }
    setIntroduction("");
  }, [onOpen, previewLogo, form]);

  const handleFinish = useCallback(
    async (data: ICompanies) => {
      setLoading(true);
      try {
        if (previewLogo) {
          const resp = await upload(previewLogo, "companies");
          if (resp.status === 201 && resp.data) {
            const respCompany = await createCompany({
              ...data,
              introduction: introduction.trim(),
              logo: resp.data.secure_url,
            });
            if (respCompany.status === 201) {
              message.success("Thêm thành công");
            }
          } else {
            message.error("Tải ảnh có lỗi. Thử lại sau!");
          }
        } else {
          const respCompany = await createCompany({
            ...data,
            introduction: introduction.trim(),
          });
          if (respCompany.status === 201) {
            message.success("Thêm thành công");
          }
        }
      } catch (error) {
        message.error("Có lỗi. Thử lại sau!");
      }
      setLoading(false);
      handleCloseModal();
    },
    [previewLogo, introduction, handleCloseModal]
  );

  return (
    <Modal
      title="Thêm công ty mới"
      open={open}
      onCancel={() => onOpen(false)}
      footer={false}
      width={700}
    >
      <Form
        form={form}
        layout="vertical"
        name="create"
        requiredMark="optional"
        autoComplete="off"
        onFinish={handleFinish}
      >
        <Form.Item
          label="Công ty"
          name="companyName"
          rules={[{ required: true, message: "Vui lòng nhập công ty" }]}
        >
          <Input placeholder="Công ty" />
        </Form.Item>
        <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
          <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
            <Form.Item
              name="provinceName"
              label="Địa điểm làm việc"
              rules={[
                { required: true, message: "Vui lòng chọn địa điểm làm việc" },
              ]}
            >
              <Select
                placeholder="Chọn tỉnh/thành phố"
                showSearch
                allowClear
                options={provinceList}
                onChange={handleChooseProvince}
              />
            </Form.Item>
          </Col>
          <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
            <Form.Item
              name="districtName"
              label="Quận/huyện"
              rules={[{ required: true, message: "Vui lòng chọn quận/huyện" }]}
            >
              <Select
                placeholder="Chọn quận/huyện"
                showSearch
                allowClear
                options={districtList}
              />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item label="Giới thiệu" required>
          <ReactQuill
            theme="snow"
            placeholder="Giới thiệu công ty"
            value={introduction}
            onChange={setIntroduction}
          />
        </Form.Item>
        <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
          <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
            <Form.Item label="Logo" required>
              <ImgCrop rotationSlider quality={1} aspect={1}>
                <Upload
                  accept="image/jpg, image/jpeg, image/png, image/webp"
                  beforeUpload={handleBeforeUpload}
                >
                  <Button icon={<UploadOutlined />} block>
                    Chọn ảnh
                  </Button>
                </Upload>
              </ImgCrop>
            </Form.Item>
          </Col>
          <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
            <Form.Item label="Quy mô" name="scales" required>
              <Input placeholder="Quy mô" />
            </Form.Item>
          </Col>
        </Row>
        {previewLogo ? (
          <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
            <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
              <Form.Item required>
                <Image
                  src={previewLogo && previewLogo.preview}
                  alt="Logo"
                  width={130}
                  height={130}
                  preview={false}
                />
              </Form.Item>
            </Col>
          </Row>
        ) : (
          false
        )}
        <Form.Item style={{ marginBottom: 0 }}>
          <Button htmlType="submit" block type="primary" loading={loading}>
            Thêm
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default memo(CreateModal);
