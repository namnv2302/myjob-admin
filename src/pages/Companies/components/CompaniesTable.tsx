import { useMemo, useState, memo, useEffect, useCallback, useRef } from "react";
import { Button, Pagination, Image, Popconfirm, message } from "antd";
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import { ProTable } from "@ant-design/pro-components";
import type { ActionType } from "@ant-design/pro-components";
import classNames from "classnames/bind";
import styles from "@pages/Companies/Companies.module.scss";
import useCompanies from "@hooks/companies/useCompanies";
import { ICompanies } from "@slices/authorization/authorizationSlice";
import { getCompaniesList, deleteCompany } from "@apis/companies";
import UpdateCompanyDrawer from "@components/UpdateCompanyDrawer";
import CreateModal from "@pages/Companies/components/CreateModal";

const cx = classNames.bind(styles);

const CompaniesTable = () => {
  const actionRef = useRef<ActionType>();
  const { data, currentPage, totalPages, loading } = useCompanies(1);
  const [current, setCurrent] = useState<number>();
  const [companiesData, setCompaniesData] = useState<ICompanies[]>();
  const [companyData, setCompanyData] = useState<ICompanies | null>(null);
  const [open, setOpen] = useState<boolean>(false);
  const [fetching, setFetching] = useState<boolean>(false);
  const [openCreateModal, setOpenCreateModal] = useState<boolean>(false);

  useEffect(() => {
    if (data) {
      setCompaniesData(data);
      setCurrent(currentPage);
    }
  }, [data, currentPage]);

  const handleBeforeOpenDrawer = useCallback((data: ICompanies) => {
    setCompanyData(data);
    setOpen(true);
  }, []);

  const handleDeleteCompany = useCallback(async (id: string) => {
    try {
      await deleteCompany(id);
      message.success("Xóa thành công, hãy cập nhật lại");
    } catch (error) {
      message.error("Có lỗi tạm thời không thể xóa");
    }
  }, []);

  const columns: any = useMemo(
    () => [
      {
        title: "Logo",
        key: "logo",
        dataIndex: "logo",
        render: (_: string, record: ICompanies) => (
          <>
            {record.logo ? (
              <Image
                src={record.logo}
                alt="Logo"
                preview={false}
                width={70}
                height={70}
              />
            ) : (
              _
            )}
          </>
        ),
      },
      {
        title: "Tên công ty",
        key: "companyName",
        dataIndex: "companyName",
        width: "32%",
        render: (companyName: string) => (
          <p className="line-clamp-1">{companyName}</p>
        ),
      },
      {
        title: "Địa chỉ",
        key: "provinceName",
        dataIndex: "provinceName",
      },
      {
        title: "Quận/huyện",
        key: "districtName",
        dataIndex: "districtName",
      },
      {
        title: "Quy mô",
        key: "scales",
        dataIndex: "scales",
        render: (scales: string) => <p className="line-clamp-1">{scales}</p>,
      },
      {
        title: "Cập nhật",
        key: "action",
        align: "center" as "center",
        render: (_: any, record: ICompanies) => (
          <>
            <Button
              type="primary"
              ghost
              icon={<EditOutlined />}
              onClick={() => handleBeforeOpenDrawer(record)}
            ></Button>
            <Popconfirm
              title="Xóa công ty"
              description="Bạn có chắc chắn không?"
              okText="Xóa"
              cancelText="Hủy"
              onConfirm={() => handleDeleteCompany(`${record.id}`)}
            >
              <Button
                danger
                icon={<DeleteOutlined color="red" />}
                className={cx("btn-delete")}
              ></Button>
            </Popconfirm>
          </>
        ),
      },
    ],
    [handleBeforeOpenDrawer, handleDeleteCompany]
  );

  const handleChangePage = useCallback(async (page: number) => {
    setFetching(true);
    try {
      const resp = await getCompaniesList(page);
      if (resp.status === 200) {
        setCompaniesData(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, []);

  const handleReload = useCallback(async () => {
    setFetching(true);
    try {
      const resp = await getCompaniesList();
      if (resp.status === 200) {
        setCompaniesData(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, []);

  return (
    <>
      <ProTable
        actionRef={actionRef}
        headerTitle="Danh sách công ty"
        rowKey="id"
        cardBordered
        search={false}
        options={false}
        loading={loading || fetching}
        columns={columns}
        dataSource={companiesData}
        pagination={false}
        toolBarRender={() => [
          <>
            <Button
              key="button-add"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => setOpenCreateModal(true)}
            >
              Thêm
            </Button>
            <Button
              key="button-reload"
              icon={<ReloadOutlined />}
              onClick={handleReload}
            >
              Tải lại
            </Button>
          </>,
        ]}
      />
      <Pagination
        style={{ marginTop: 26, textAlign: "center" }}
        current={current}
        pageSize={10}
        total={10 * totalPages}
        onChange={handleChangePage}
      />

      <UpdateCompanyDrawer company={companyData} open={open} onOpen={setOpen} />

      <CreateModal open={openCreateModal} onOpen={setOpenCreateModal} />
    </>
  );
};

export default memo(CompaniesTable);
