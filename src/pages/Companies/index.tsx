import { useEffect, useMemo, useRef, useState } from "react";
import classNames from "classnames/bind";
import { Button, Descriptions, Typography, Image } from "antd";
import type { DescriptionsProps } from "antd";
import { useAppSelector } from "redux/hooks";
import styles from "@pages/Companies/Companies.module.scss";
import { AdminRole } from "@slices/authorization/authorizationSlice";
import CompaniesTable from "@pages/Companies/components/CompaniesTable";
import UpdateCompanyDrawer from "@components/UpdateCompanyDrawer";

const cx = classNames.bind(styles);

const CompaniesPage = () => {
  const authorization = useAppSelector((state) => state.authorization);
  const introductionRef = useRef<HTMLPreElement | any>();
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);

  useEffect(() => {
    if (introductionRef && introductionRef.current) {
      if (!authorization?.company?.introduction?.trim()) {
        introductionRef.current.innerText = "Chưa có";
      } else {
        introductionRef.current.innerHTML =
          authorization?.company?.introduction;
      }
    }
  }, [authorization?.company?.introduction]);

  const items: DescriptionsProps["items"] = useMemo(
    () => [
      {
        key: "1",
        label: "Tên công ty",
        children: (
          <Typography.Text className={cx("value")}>
            {authorization?.company?.companyName || "Chưa có"}
          </Typography.Text>
        ),
      },
      {
        key: "2",
        label: "Địa chỉ",
        children: (
          <Typography.Text className={cx("value")}>
            {authorization?.company?.provinceName || "Chưa có"}
          </Typography.Text>
        ),
      },
      {
        key: "3",
        label: "Quận/huyện",
        children: (
          <Typography.Text className={cx("value")}>
            {authorization?.company?.districtName || "Chưa có"}
          </Typography.Text>
        ),
      },
      {
        key: "4",
        label: "Giới thiệu",
        span: { xl: 3, xxl: 3 },
        children: (
          <Typography.Paragraph style={{ marginBottom: 0 }}>
            <pre ref={introductionRef} style={{ margin: 0 }}></pre>
          </Typography.Paragraph>
        ),
      },
      {
        key: "5",
        label: "Logo",
        children: (
          <>
            {authorization && authorization.company?.logo ? (
              <Image
                src={authorization?.company?.logo}
                alt="Logo"
                width={150}
                height={150}
                preview={false}
              />
            ) : (
              <Typography.Text className={cx("value")}>
                Thêm logo công ty
              </Typography.Text>
            )}
          </>
        ),
      },
      {
        key: "6",
        label: "Quy mô",
        children: (
          <Typography.Text className={cx("value")}>
            {authorization?.company?.scales || "Chưa có"}
          </Typography.Text>
        ),
      },
    ],
    [authorization]
  );

  return (
    <>
      {authorization && authorization?.role === AdminRole[0] ? (
        <CompaniesTable />
      ) : (
        <>
          <Descriptions
            title={
              <Typography.Text className={cx("title")}>
                Thông tin công ty
              </Typography.Text>
            }
            size="default"
            extra={
              <Button type="primary" onClick={() => setOpenDrawer(true)}>
                Chỉnh sửa
              </Button>
            }
            items={items}
          />
          {authorization?.company ? (
            <UpdateCompanyDrawer
              company={authorization?.company}
              open={openDrawer}
              onOpen={setOpenDrawer}
            />
          ) : (
            false
          )}
        </>
      )}
    </>
  );
};

export default CompaniesPage;
