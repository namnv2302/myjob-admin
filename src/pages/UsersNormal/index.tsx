import { Role } from "@constants/common";
import UsersTable from "@pages/UsersNormal/components/UsersTable";
import { useAppSelector } from "redux/hooks";
import NotPermission from "@components/NotPermission";

const UsersNormalPage = () => {
  const authorization = useAppSelector((state) => state.authorization);

  return (
    <>
      {authorization?.role === Role[2].key ? (
        <UsersTable />
      ) : (
        <NotPermission
          status="403"
          title="403"
          subTitle="Bạn không được phép truy cập trang này"
        />
      )}
    </>
  );
};

export default UsersNormalPage;
