import { memo, useCallback, useEffect, useState } from "react";
import {
  Button,
  Col,
  Drawer,
  Form,
  Image,
  Input,
  Row,
  Select,
  Upload,
  message,
} from "antd";
import axios from "axios";
import { UploadOutlined } from "@ant-design/icons";
import ImgCrop from "antd-img-crop";
import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import { Role, Gender } from "@constants/common";
import { upload } from "@apis/upload";
import { updateByAdmin } from "@apis/users";

const UpdateDrawer = ({
  data,
  open,
  onOpen,
}: {
  data: AuthorizationData | null;
  open: boolean;
  onOpen: any;
}) => {
  const [form] = Form.useForm();
  const [previewAvatar, setPreviewAvatar] = useState<any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    form.setFieldsValue({
      fullname: data?.fullname,
      email: data?.email,
      phoneNumber: data?.phoneNumber,
      gender: data?.gender,
      role: data?.role,
    });
  }, [form, data]);

  const handleCloseDrawer = useCallback(() => {
    onOpen(false);
    form.resetFields();
    if (previewAvatar) {
      URL.revokeObjectURL(previewAvatar.preview);
      setPreviewAvatar(null);
    }
  }, [onOpen, form, previewAvatar]);

  const handleBeforeUpload = useCallback(async (file: any) => {
    file.preview = URL.createObjectURL(file);
    setPreviewAvatar(file);
  }, []);

  const handleFinish = useCallback(
    async (dataForm: AuthorizationData) => {
      delete dataForm.email;
      setLoading(true);
      try {
        if (previewAvatar) {
          const resp = await upload(previewAvatar, "users");
          if (resp.status === 201 && resp.data) {
            const respUser = await updateByAdmin(`${data?.id}`, {
              ...dataForm,
              avatar: resp.data.secure_url,
            });
            if (respUser.status === 200) {
              message.success("Cập nhật thành công");
            }
          } else {
            message.error("Tải ảnh có lỗi. Thử lại sau!");
          }
        } else {
          const respUser = await updateByAdmin(`${data?.id}`, {
            ...dataForm,
          });
          if (respUser.status === 200) {
            message.success("Cập nhật thành công");
          }
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error("Có lỗi. Thử lại sau!");
        }
      }
      handleCloseDrawer();
      setLoading(false);
    },
    [handleCloseDrawer, previewAvatar, data?.id]
  );

  return (
    <Drawer
      title="Chỉnh sửa thông tin"
      placement="right"
      width={500}
      open={open}
      forceRender
      onClose={handleCloseDrawer}
    >
      <Form
        form={form}
        layout="vertical"
        requiredMark="optional"
        initialValues={{
          fullname: data?.fullname,
          email: data?.email,
          phoneNumber: data?.phoneNumber,
          gender: data?.gender,
          role: data?.role,
        }}
        onFinish={handleFinish}
      >
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="fullname"
              label="Họ tên"
              rules={[{ required: true, message: "Họ tên không được trống" }]}
            >
              <Input placeholder="Họ tên" />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item name="email" label="Email" required>
              <Input placeholder="Email" disabled />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="phoneNumber"
              label="Số điện thoại"
              rules={[
                { required: true, message: "Số điện thoại không được trống" },
              ]}
            >
              <Input placeholder="Số điện thoại" />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="gender"
              label="Giới tính"
              rules={[
                { required: true, message: "Giới tính không được trống" },
              ]}
            >
              <Select
                placeholder="Chọn giới tính"
                options={Gender.map(({ key, label }) => ({
                  value: key,
                  label,
                }))}
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item label="Ảnh đại diện" required>
              <ImgCrop rotationSlider quality={1} aspect={1}>
                <Upload
                  accept="image/jpg, image/jpeg, image/png"
                  beforeUpload={handleBeforeUpload}
                >
                  <Button icon={<UploadOutlined />} block>
                    Chọn ảnh
                  </Button>
                </Upload>
              </ImgCrop>
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="role"
              label="Vai trò"
              rules={[{ required: true, message: "Vai trò không được trống" }]}
            >
              <Select
                placeholder="Chọn vai trò"
                options={Role.map(({ key, label }) => ({
                  value: key,
                  label,
                }))}
              />
            </Form.Item>
          </Col>
        </Row>
        {previewAvatar || data?.avatar ? (
          <Row gutter={{ xl: 16 }}>
            <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
              <Form.Item required>
                <Image
                  src={(previewAvatar && previewAvatar.preview) || data?.avatar}
                  alt="Avatar"
                  width={150}
                  height={150}
                  preview={false}
                />
              </Form.Item>
            </Col>
          </Row>
        ) : (
          false
        )}
        <Row>
          <Col xl={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item style={{ marginBottom: 0 }}>
              <Button type="primary" block htmlType="submit" loading={loading}>
                Hoàn tất
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};

export default memo(UpdateDrawer);
