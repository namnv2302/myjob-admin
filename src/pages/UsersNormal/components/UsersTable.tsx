import { useMemo, memo, useState, useEffect, useCallback } from "react";
import { ProTable } from "@ant-design/pro-components";
// import type { ProColumns } from "@ant-design/pro-components";
import { Button, Tag, Popconfirm, Pagination, message, Image } from "antd";
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  ReloadOutlined,
  MailOutlined,
  PhoneOutlined,
} from "@ant-design/icons";
import useUsersNormal from "@hooks/users/useUsersNormal";
import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import { deleteUser, getUsersNormalList } from "@apis/users";
import UpdateDrawer from "@pages/UsersNormal/components/UpdateDrawer";
import CreateModal from "@pages/UsersNormal/components/CreateModal";

// type DataType = {
//   id: number;
//   avatar: string;
//   fullname: string;
//   email: string;
//   phoneNumber: string;
//   isVerify: boolean;
// };

const UsersTable = () => {
  const { data, loading, currentPage, totalPages } = useUsersNormal(1);
  const [current, setCurrent] = useState<number>();
  const [usersData, setUsersData] = useState<AuthorizationData[]>();
  const [fetching, setFetching] = useState<boolean>(false);
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);
  const [userData, setUserData] = useState<AuthorizationData | null>(null);
  const [openCreateModal, setOpenCreateModal] = useState<boolean>(false);

  useEffect(() => {
    if (data) {
      setUsersData(data);
      setCurrent(currentPage);
    }
  }, [data, currentPage]);

  const handleBeforeOpenDrawer = useCallback((data: AuthorizationData) => {
    setOpenDrawer(true);
    setUserData(data);
  }, []);

  const handleDeleteUser = useCallback(async (id: string) => {
    try {
      await deleteUser(id);
      message.success("Xóa thành công, hãy cập nhật lại");
    } catch (error) {
      message.error("Có lỗi tạm thời không thể xóa");
    }
  }, []);

  const columns: any = useMemo(
    () => [
      {
        title: "Ảnh",
        key: "avatar",
        dataIndex: "avatar",
        render: (_: string, record: AuthorizationData) => {
          return (
            <>
              {record.avatar ? (
                <Image
                  src={record.avatar}
                  alt="Logo"
                  preview={false}
                  width={50}
                  height={50}
                  style={{ borderRadius: "100%" }}
                />
              ) : (
                _
              )}
            </>
          );
        },
      },
      {
        title: "Họ tên",
        key: "fullname",
        dataIndex: "fullname",
      },
      {
        title: "Địa chỉ email",
        key: "email",
        dataIndex: "email",
        render: (email: string) => (
          <p style={{ display: "flex", gap: "8px" }}>
            <MailOutlined />
            <span>{email}</span>
          </p>
        ),
      },
      {
        title: "Số điện thoại",
        key: "phoneNumber",
        dataIndex: "phoneNumber",
        render: (phoneNumber: string) => (
          <p style={{ display: "flex", gap: "8px" }}>
            <PhoneOutlined />
            <span>{phoneNumber}</span>
          </p>
        ),
      },
      {
        title: "Xác thực",
        key: "isVerify",
        dataIndex: "isVerify",
        render: (value: boolean) =>
          value ? (
            <Tag color="success" className="tag">
              Đã xác thực
            </Tag>
          ) : (
            <Tag color="error" className="tag">
              Chưa xác thực
            </Tag>
          ),
      },
      {
        title: "Cập nhật",
        key: "action",
        align: "center" as "center",
        render: (_: any, record: AuthorizationData) => (
          <>
            <Button
              type="primary"
              ghost
              icon={<EditOutlined />}
              onClick={() => handleBeforeOpenDrawer(record)}
            ></Button>
            <Popconfirm
              title="Xóa ứng viên"
              description="Bạn có chắc chắn không?"
              okText="Xóa"
              cancelText="Hủy"
              onConfirm={() => handleDeleteUser(`${record.id}`)}
            >
              <Button
                danger
                icon={<DeleteOutlined color="red" />}
                style={{ marginLeft: 8 }}
              ></Button>
            </Popconfirm>
          </>
        ),
      },
    ],
    [handleBeforeOpenDrawer, handleDeleteUser]
  );

  const handleChangePage = useCallback(async (page: number) => {
    setFetching(true);
    try {
      const resp = await getUsersNormalList(page);
      if (resp.status === 200) {
        setUsersData(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, []);

  const handleReload = useCallback(async () => {
    setFetching(true);
    try {
      const resp = await getUsersNormalList();
      if (resp.status === 200) {
        setUsersData(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, []);

  return (
    <>
      <ProTable
        rowKey="id"
        headerTitle="Danh sách ứng viên"
        cardBordered
        search={false}
        options={false}
        pagination={false}
        loading={loading || fetching}
        columns={columns}
        dataSource={usersData}
        toolBarRender={() => [
          <>
            <Button
              key="button-add"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => setOpenCreateModal(true)}
            >
              Thêm
            </Button>
            <Button
              key="button-reload"
              icon={<ReloadOutlined />}
              onClick={handleReload}
            >
              Tải lại
            </Button>
          </>,
        ]}
      />
      <Pagination
        style={{ marginTop: 26, textAlign: "center" }}
        current={current}
        pageSize={10}
        total={10 * totalPages}
        onChange={handleChangePage}
      />

      <UpdateDrawer open={openDrawer} onOpen={setOpenDrawer} data={userData} />

      <CreateModal open={openCreateModal} onOpen={setOpenCreateModal} />
    </>
  );
};

export default memo(UsersTable);
