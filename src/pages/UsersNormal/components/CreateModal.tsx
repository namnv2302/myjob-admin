import { memo, useCallback, useState } from "react";
import axios from "axios";
import {
  Button,
  Col,
  Form,
  Image,
  Input,
  Modal,
  Row,
  Select,
  Upload,
  message,
} from "antd";
import ImgCrop from "antd-img-crop";
import { UploadOutlined } from "@ant-design/icons";
import { Gender, phoneExp } from "@constants/common";
import { AuthorizationData } from "@slices/authorization/authorizationSlice";
import { upload } from "@apis/upload";
import { createNormalUser } from "@apis/users";

const CreateModal = ({ open, onOpen }: { open: boolean; onOpen: any }) => {
  const [form] = Form.useForm();
  const [previewAvatar, setPreviewAvatar] = useState<any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const handleBeforeUpload = useCallback(async (file: any) => {
    file.preview = URL.createObjectURL(file);
    setPreviewAvatar(file);
  }, []);

  const handleCloseModal = useCallback(() => {
    onOpen(false);
    form.resetFields();
    if (previewAvatar) {
      URL.revokeObjectURL(previewAvatar.preview);
      setPreviewAvatar(null);
    }
  }, [onOpen, previewAvatar, form]);

  const handleFinish = useCallback(
    async (data: AuthorizationData) => {
      setLoading(true);
      try {
        if (previewAvatar) {
          const resp = await upload(previewAvatar, "users");
          if (resp.status === 201 && resp.data) {
            const respUser = await createNormalUser({
              ...data,
              avatar: resp.data.secure_url,
            });
            if (respUser.status === 201) {
              message.success("Tạo thành công");
            }
          } else {
            message.error("Tải ảnh có lỗi. Thử lại sau!");
          }
        } else {
          const respUser = await createNormalUser({ ...data });
          if (respUser.status === 201) {
            message.success("Tạo thành công");
          }
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error("Có lỗi. Thử lại sau!");
        }
      }
      setLoading(false);
      handleCloseModal();
    },
    [previewAvatar, handleCloseModal]
  );

  return (
    <Modal
      title="Thêm ứng viên mới"
      open={open}
      onCancel={() => onOpen(false)}
      footer={false}
    >
      <Form
        form={form}
        name="create"
        requiredMark="optional"
        autoComplete="off"
        layout="vertical"
        onFinish={handleFinish}
      >
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="fullname"
              label="Họ tên"
              rules={[{ required: true, message: "Họ tên không được trống" }]}
            >
              <Input placeholder="Họ tên" />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="email"
              label="Email"
              rules={[
                { required: true, message: "Email không được trống" },
                { type: "email", message: "Không đúng định dạng email" },
              ]}
            >
              <Input placeholder="Email" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="password"
              label="Mật khẩu"
              rules={[{ required: true, message: "Mật khẩu không được trống" }]}
            >
              <Input.Password placeholder="Mật khẩu" />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="phoneNumber"
              label="Số điện thoại"
              required
              rules={[
                {
                  validator(_, value) {
                    if (phoneExp.test(value)) {
                      return Promise.resolve();
                    }
                    return Promise.reject("Số điện thoại phải đủ 10 số");
                  },
                },
              ]}
            >
              <Input placeholder="Số điện thoại" />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ xl: 16 }}>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item
              name="gender"
              label="Giới tính"
              rules={[
                { required: true, message: "Giới tính không được trống" },
              ]}
            >
              <Select
                placeholder="Chọn giới tính"
                options={Gender.map(({ key, label }) => ({
                  value: key,
                  label,
                }))}
              />
            </Form.Item>
          </Col>
          <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item label="Ảnh đại diện" required>
              <ImgCrop rotationSlider quality={1} aspect={1}>
                <Upload
                  accept="image/jpg, image/jpeg, image/png"
                  beforeUpload={handleBeforeUpload}
                >
                  <Button icon={<UploadOutlined />} block>
                    Chọn ảnh
                  </Button>
                </Upload>
              </ImgCrop>
            </Form.Item>
          </Col>
        </Row>
        {previewAvatar ? (
          <Row gutter={{ xl: 16 }}>
            <Col xl={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
              <Form.Item required>
                <Image
                  src={previewAvatar.preview}
                  alt="Avatar"
                  width={130}
                  height={130}
                  preview={false}
                />
              </Form.Item>
            </Col>
          </Row>
        ) : (
          false
        )}
        <Row>
          <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item style={{ marginBottom: 0 }}>
              <Button type="primary" block htmlType="submit" loading={loading}>
                Thêm
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default memo(CreateModal);
