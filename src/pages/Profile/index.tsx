import { useCallback, useEffect, useState } from "react";
import {
  Button,
  Card,
  Col,
  Form,
  Image,
  Input,
  Row,
  Select,
  Upload,
  message,
} from "antd";
import axios from "axios";
import { UploadOutlined } from "@ant-design/icons";
import ImgCrop from "antd-img-crop";
import { Gender } from "@constants/common";
import { useAppSelector, useAppDispatch } from "redux/hooks";
import {
  AuthorizationData,
  updateProfile,
} from "@slices/authorization/authorizationSlice";
import { upload } from "@apis/upload";
import { update } from "@apis/users";

const ProfilePage = () => {
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const authorization = useAppSelector((state) => state.authorization);
  const [previewAvatar, setPreviewAvatar] = useState<any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    form.setFieldsValue({
      fullname: authorization?.fullname,
      email: authorization?.email,
      gender: authorization?.gender,
      phoneNumber: authorization?.phoneNumber,
    });
  }, [form, authorization]);

  const handleBeforeUpload = useCallback(async (file: any) => {
    file.preview = URL.createObjectURL(file);
    setPreviewAvatar(file);
  }, []);

  const handleFinish = useCallback(
    async (formData: AuthorizationData) => {
      delete formData.email;
      setLoading(true);
      try {
        if (previewAvatar) {
          const resp = await upload(previewAvatar, "users");
          if (resp.status === 201 && resp.data) {
            const respUser = await update(`${authorization?.id}`, {
              ...formData,
              avatar: resp.data.secure_url,
            });
            if (respUser.status === 200) {
              message.success("Cập nhật thành công");
              dispatch(updateProfile(respUser.data));
            }
          } else {
            message.error("Tải ảnh có lỗi. Thử lại sau!");
          }
          URL.revokeObjectURL(previewAvatar.preview);
          setPreviewAvatar(null);
        } else {
          const respUser = await update(`${authorization?.id}`, {
            ...formData,
            avatar: authorization?.avatar,
          });
          if (respUser.status === 200) {
            message.success("Cập nhật thành công");
            dispatch(updateProfile(respUser.data));
          }
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error("Có lỗi. Thử lại sau!");
        }
      }
      setLoading(false);
    },
    [authorization?.id, previewAvatar, dispatch, authorization?.avatar]
  );

  return (
    <Row>
      <Col
        xl={{ span: 12, offset: 6 }}
        lg={{ span: 24, offset: 0 }}
        sm={{ span: 24, offset: 0 }}
        xs={{ span: 24, offset: 0 }}
      >
        <Card title="Thông tin cá nhân">
          <Form
            form={form}
            name="profile"
            layout="vertical"
            requiredMark="optional"
            initialValues={{
              fullname: authorization?.fullname,
              email: authorization?.email,
              gender: authorization?.gender,
              phoneNumber: authorization?.phoneNumber,
            }}
            onFinish={handleFinish}
          >
            <Row gutter={{ lg: 16, sm: 12 }}>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
                <Form.Item
                  name="fullname"
                  label="Họ tên"
                  rules={[
                    { required: true, message: "Họ tên không được trống" },
                  ]}
                >
                  <Input placeholder="Họ tên" />
                </Form.Item>
              </Col>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
                <Form.Item label="Email" name="email" required>
                  <Input placeholder="Email" disabled />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ lg: 16, sm: 12 }}>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
                <Form.Item
                  name="gender"
                  label="Giới tính"
                  rules={[
                    { required: true, message: "Giới tính không được trống" },
                  ]}
                >
                  <Select
                    placeholder="Chọn giới tính"
                    options={Gender.map(({ key, label }) => ({
                      value: key,
                      label,
                    }))}
                  />
                </Form.Item>
              </Col>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
                <Form.Item
                  name="phoneNumber"
                  label="Số điện thoại"
                  rules={[
                    {
                      required: true,
                      message: "Số điện thoại không được trống",
                    },
                  ]}
                >
                  <Input placeholder="Số điện thoại" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={{ lg: 16, sm: 12 }}>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
                <Form.Item label="Ảnh đại diện" required>
                  <ImgCrop rotationSlider quality={1} aspect={1}>
                    <Upload
                      accept="image/jpg, image/jpeg, image/png"
                      beforeUpload={handleBeforeUpload}
                    >
                      <Button icon={<UploadOutlined />} block>
                        Chọn ảnh
                      </Button>
                    </Upload>
                  </ImgCrop>
                </Form.Item>
              </Col>
              <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
                {previewAvatar || authorization?.avatar ? (
                  <Form.Item label="Xem trước" required>
                    <Image
                      src={
                        (previewAvatar && previewAvatar.preview) ||
                        authorization?.avatar
                      }
                      alt="Avatar"
                      width={150}
                      height={150}
                      preview={false}
                    />
                  </Form.Item>
                ) : (
                  false
                )}
              </Col>
            </Row>
            <Row>
              <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
                <Form.Item style={{ marginBottom: 0 }}>
                  <Button
                    type="primary"
                    block
                    htmlType="submit"
                    loading={loading}
                  >
                    Chỉnh sửa
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
      </Col>
    </Row>
  );
};

export default ProfilePage;
