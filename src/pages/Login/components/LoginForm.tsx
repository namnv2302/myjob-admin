import { useCallback, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { Button, Form, Input, message } from "antd";
import classNames from "classnames/bind";
import styles from "@pages/Login/Login.module.scss";
import { login, whoAmI } from "@apis/auth";
import { ROUTE_PATH } from "@constants/routes";
import { saveAccessToken, saveRefreshToken } from "@utils/localstorage";
import { useAppDispatch } from "redux/hooks";
import {
  login as loginAction,
  AdminRole,
} from "@slices/authorization/authorizationSlice";

const cx = classNames.bind(styles);

const LoginForm = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  const handleFinish = useCallback(
    async ({ email, password }: { email: string; password: string }) => {
      setLoading(true);
      try {
        const resp = await login(email, password);
        if (resp.status === 201 && resp.data) {
          saveAccessToken(resp.data.accessToken);
          saveRefreshToken(resp.data.refreshToken);

          const respAuth = await whoAmI();
          if (respAuth.data && AdminRole.includes(respAuth.data.role)) {
            dispatch(loginAction(respAuth.data));
            navigate(ROUTE_PATH.DASHBOARD);
            message.success("Chào mừng bạn quay lại!!");
          } else {
            message.warning("Bạn không có quyền truy cập!!");
          }
        } else {
          message.warning("Thông tin không chính xác!!");
        }
        setLoading(false);
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error("Có lỗi, thử lại sau!!");
        }
        setLoading(false);
      }
    },
    [navigate, dispatch]
  );

  return (
    <div className={cx("login-form")}>
      <Form
        layout="vertical"
        requiredMark="optional"
        form={form}
        onFinish={handleFinish}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[
            { type: "email", message: "Nhập đúng định dạng email" },
            { required: true, message: "Vui lòng nhập email" },
          ]}
        >
          <Input placeholder="Email" />
        </Form.Item>
        <Form.Item
          label="Mật khẩu"
          name="password"
          rules={[{ required: true, message: "Vui lòng nhập mật khẩu" }]}
        >
          <Input.Password placeholder="Mật khẩu" />
        </Form.Item>
        <Form.Item>
          <Button htmlType="submit" type="primary" block loading={loading}>
            Đăng nhập
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default LoginForm;
