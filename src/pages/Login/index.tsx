import { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Typography, Image } from "antd";
import classNames from "classnames/bind";
import styles from "@pages/Login/Login.module.scss";
import { ROUTE_PATH } from "@constants/routes";
import images from "@assets/images";
import LoginForm from "@pages/Login/components/LoginForm";
import { useAppSelector } from "redux/hooks";

const cx = classNames.bind(styles);

const LoginPage = () => {
  const navigate = useNavigate();
  const authorization = useAppSelector((state) => state.authorization);

  useEffect(() => {
    if (authorization) {
      navigate(`${ROUTE_PATH.HOME}`);
    }
  }, [navigate, authorization]);

  return (
    <div className={cx("wrapper")}>
      <Link to={ROUTE_PATH.HOME} className={cx("logo")}>
        <Image
          src={images.logoImage}
          preview={false}
          alt="Logo"
          className={cx("logo-image")}
        />
        <Typography.Title level={5} className={cx("logo-title")}>
          MyJob
        </Typography.Title>
      </Link>
      <Typography.Text className={cx("title")}>
        Chào mừng bạn đã quay trở lại
      </Typography.Text>
      <Typography.Text className={cx("sub-title")}>
        Cùng tạo dựng lợi thế cho doanh nghiệp bằng trải nghiệm công nghệ tuyển
        dụng ứng dụng sâu AI & Hiring Funnel
      </Typography.Text>
      <LoginForm />
      <Typography.Text style={{ display: "block", textAlign: "center" }}>
        Bạn chưa có tài khoản?{" "}
        <Link to={ROUTE_PATH.EMPLOYER_SIGN_UP} className={cx("now")}>
          Đăng ký ngay
        </Link>
      </Typography.Text>
    </div>
  );
};

export default LoginPage;
