import Banner from "@pages/Home/components/Banner";
import Services from "@pages/Home/components/Services";

const HomePage = () => {
  return (
    <>
      <Banner />
      <Services />
    </>
  );
};

export default HomePage;
