import { useNavigate } from "react-router-dom";
import { Button, Typography } from "antd";
import { ArrowRightOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./Banner.module.scss";
import images from "@assets/images";
import { ROUTE_PATH } from "@constants/routes";

const cx = classNames.bind(styles);

const Banner = () => {
  const navigate = useNavigate();

  return (
    <div
      style={{ backgroundImage: `url(${images.backgroundHeader})` }}
      className={cx("wrapper")}
    >
      <div className={cx("content")}>
        <Typography.Text className={cx("title")}>
          Đăng tin tuyển dụng,
        </Typography.Text>
        <Typography.Text className={cx("title")}>
          tìm kiếm ứng viên hiệu quả
        </Typography.Text>
        <div style={{ textAlign: "center", marginTop: "10px" }}>
          <Button
            type="primary"
            icon={<ArrowRightOutlined />}
            onClick={() => navigate(ROUTE_PATH.EMPLOYER_SIGN_UP)}
          >
            Đăng tin miễn phí
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Banner;
