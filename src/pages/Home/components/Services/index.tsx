import { Typography, Row, Col, Image } from "antd";
import { CheckCircleOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./Services.module.scss";
import images from "@assets/images";

const cx = classNames.bind(styles);

const Services = () => {
  return (
    <div id="services" className={cx("wrapper")}>
      <div className={cx("employer")}>
        <div className={cx("inner")}>
          <Typography.Text className={cx("title")}>
            RECRUITMENT SERVICES
          </Typography.Text>
          <Typography.Text className={cx("heading")}>
            Dịch vụ đăng tin tuyển dụng
          </Typography.Text>
          <Row gutter={{ lg: 16 }} align="middle">
            <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
              <Image
                className={cx("image-user")}
                src={images.freelancerIcon}
                preview={false}
                alt="Icon"
              />
            </Col>
            <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
              <Typography.Text className={cx("heading-2")}>
                Đăng tin tuyển dụng miễn phí
              </Typography.Text>
              <div className={cx("item")}>
                <CheckCircleOutlined className={cx("icon")} />
                <Typography.Text>
                  Đăng tin tuyển dụng miễn phí và không giới hạn số lượng.
                </Typography.Text>
              </div>
              <div className={cx("item")}>
                <CheckCircleOutlined className={cx("icon")} />
                <Typography.Text>
                  Đăng tin tuyển dụng dễ dàng, không quá 1 phút.
                </Typography.Text>
              </div>
              <div className={cx("item")}>
                <CheckCircleOutlined className={cx("icon")} />
                <Typography.Text>
                  Tiếp cận nguồn CV ứng viên khổng lồ, tìm kiếm ứng viên từ kho
                  dữ liệu hơn 5 triệu hồ sơ.
                </Typography.Text>
              </div>
              <div className={cx("item")}>
                <CheckCircleOutlined className={cx("icon")} />
                <Typography.Text>
                  Dễ dàng kiểm duyệt và đăng tin trong 24h.
                </Typography.Text>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default Services;
