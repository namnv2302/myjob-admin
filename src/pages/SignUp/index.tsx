import { useEffect, useMemo } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Typography, Collapse, Image } from "antd";
import type { CollapseProps } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "@pages/SignUp/SignUp.module.scss";
import { ROUTE_PATH } from "@constants/routes";
import images from "@assets/images";
import SignUpForm from "@pages/SignUp/components/SignUpForm";
import { useAppSelector } from "redux/hooks";

const cx = classNames.bind(styles);

const SignUpPage = () => {
  const navigate = useNavigate();
  const authorization = useAppSelector((state) => state.authorization);

  useEffect(() => {
    if (authorization) {
      navigate(`${ROUTE_PATH.HOME}`);
    }
  }, [navigate, authorization]);

  const items: CollapseProps["items"] = useMemo(
    () => [
      {
        key: "1",
        label: (
          <Typography.Text className={cx("regulations")}>
            Quy định
          </Typography.Text>
        ),
        children: (
          <Typography.Text>
            Để đảm bảo chất lượng dịch vụ, MyJob không cho phép một người dùng
            tạo nhiều tài khoản khác nhau.
          </Typography.Text>
        ),
      },
    ],
    []
  );

  return (
    <div className={cx("wrapper")}>
      <Link to={ROUTE_PATH.HOME} className={cx("logo")}>
        <Image
          src={images.logoImage}
          preview={false}
          alt="Logo"
          className={cx("logo-image")}
        />
        <Typography.Title level={5} className={cx("logo-title")}>
          MyJob
        </Typography.Title>
      </Link>
      <Typography.Text className={cx("title")}>
        Đăng ký tài khoản Nhà tuyển dụng
      </Typography.Text>
      <Typography.Text className={cx("sub-title")}>
        Cùng tạo dựng lợi thế cho doanh nghiệp bằng trải nghiệm công nghệ tuyển
        dụng ứng dụng sâu AI & Hiring Funnel.
      </Typography.Text>
      <Collapse
        bordered={false}
        expandIcon={({ isActive }) => (
          <CaretRightOutlined rotate={isActive ? 90 : 0} />
        )}
        items={items}
      />
      <SignUpForm />
      <Typography.Text
        style={{ display: "block", textAlign: "center", marginBottom: 60 }}
      >
        Đã có tài khoản?{" "}
        <Link to={ROUTE_PATH.EMPLOYER_LOGIN} className={cx("now")}>
          Đăng nhập ngay
        </Link>
      </Typography.Text>
    </div>
  );
};

export default SignUpPage;
