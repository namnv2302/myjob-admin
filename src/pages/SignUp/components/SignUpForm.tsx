import { useCallback, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import {
  Form,
  Input,
  Row,
  Typography,
  Col,
  Select,
  Button,
  message,
} from "antd";
import classNames from "classnames/bind";
import styles from "@pages/SignUp/SignUp.module.scss";
import { signUpEmployer } from "@apis/auth";
import { ROUTE_PATH } from "@constants/routes";
import { phoneExp } from "@constants/common";

const cx = classNames.bind(styles);

export type ProvinceType = {
  province_name?: string;
  province_id?: number;
};

export type DistrictType = {
  district_name?: string;
  district_id?: number;
  province_id?: number;
};

export type SignUpEmployerDataType = {
  email?: string;
  fullname?: string;
  password?: string;
  ["password-confirm"]?: string;
  phoneNumber?: string;
  companyName?: string;
  provinceName?: string;
  districtName?: string;
};

const SignUpForm = () => {
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [provinceList, setProvinceList] = useState<ProvinceType[]>([]);
  const [districtList, setDistrictList] = useState<DistrictType[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    (async () => {
      const resp = await axios.get(
        `${process.env.REACT_APP_PROVINCE_BASE_URL}/province`
      );
      if (resp.status === 200) {
        const list = resp.data.results.map((item: ProvinceType) => ({
          key: item.province_id,
          value: item.province_name,
          label: item.province_name,
        }));
        setProvinceList(list);
      }
    })();
  }, []);

  const handleChooseProvince = useCallback(
    async (value: string, option: any) => {
      if (option) {
        const resp = await axios.get(
          `${process.env.REACT_APP_PROVINCE_BASE_URL}/province/district/${option.key}`
        );
        if (resp.status === 200) {
          const list = resp.data.results.map((item: DistrictType) => ({
            value: item.district_name,
            label: item.district_name,
          }));

          setDistrictList(list);
        }
      }
    },
    []
  );

  const handleFinish = useCallback(
    async (data: SignUpEmployerDataType) => {
      setLoading(true);
      try {
        delete data["password-confirm"];
        const resp = await signUpEmployer(data);
        if (resp.status === 201 && resp.data) {
          navigate(ROUTE_PATH.EMPLOYER_LOGIN);
          message.success("Tạo tài khoản thành công. Đăng nhập ngay!!");
        } else {
          message.warning("Thông tin đã tồn tại!!");
        }
        setLoading(false);
      } catch (error) {
        setLoading(false);
        message.error("Có lỗi, thử lại sau!!");
      }
    },
    [navigate]
  );

  return (
    <div className={cx("sign-up-form")}>
      <Form
        layout="vertical"
        requiredMark="optional"
        form={form}
        onFinish={handleFinish}
      >
        <div className={cx("account-title")}>
          <span className={cx("lineY")}></span>
          <Typography.Title level={5} style={{ marginBottom: 0 }}>
            Tài khoản
          </Typography.Title>
        </div>
        <Form.Item
          name="email"
          label="Email"
          rules={[
            { type: "email", message: "Nhập đúng định dạng email" },
            { required: true, message: "Vui lòng nhập email" },
          ]}
        >
          <Input placeholder="Email" />
        </Form.Item>
        <Form.Item
          label="Mật khẩu"
          name="password"
          hasFeedback
          rules={[{ required: true, message: "Vui lòng nhập mật khẩu" }]}
        >
          <Input.Password placeholder="Mật khẩu" />
        </Form.Item>
        <Form.Item
          name="password-confirm"
          label="Nhập lại mật khẩu"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Vui lòng nhập lại mật khẩu",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error("Mật khẩu không khớp"));
              },
            }),
          ]}
        >
          <Input.Password placeholder="Nhập lại mật khẩu" />
        </Form.Item>
        <div className={cx("account-title")}>
          <span className={cx("lineY")}></span>
          <Typography.Title level={5} style={{ marginBottom: 0 }}>
            Thông tin nhà tuyển dụng
          </Typography.Title>
        </div>
        <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
          <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
            <Form.Item
              label="Họ và tên"
              name="fullname"
              rules={[{ required: true, message: "Vui lòng nhập họ và tên" }]}
            >
              <Input placeholder="Họ và tên" />
            </Form.Item>
          </Col>
          <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
            <Form.Item
              label="Số điện thoại cá nhân"
              name="phoneNumber"
              required
              rules={[
                // { required: true, message: "Số điện thoại không được trống" },
                {
                  validator(_, value) {
                    if (phoneExp.test(value)) {
                      return Promise.resolve();
                    }
                    return Promise.reject("Số điện thoại phải đủ 10 số");
                  },
                },
              ]}
            >
              <Input placeholder="Số điện thoại" />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item
          label="Công ty"
          name="companyName"
          rules={[{ required: true, message: "Vui lòng nhập công ty" }]}
        >
          <Input placeholder="Công ty" />
        </Form.Item>
        <Row gutter={{ lg: 16, sm: 12, xs: 8 }}>
          <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
            <Form.Item
              name="provinceName"
              label="Địa điểm làm việc"
              rules={[
                { required: true, message: "Vui lòng chọn địa điểm làm việc" },
              ]}
            >
              <Select
                placeholder="Chọn tỉnh/thành phố"
                showSearch
                allowClear
                options={provinceList}
                onChange={handleChooseProvince}
              />
            </Form.Item>
          </Col>
          <Col lg={{ span: 12 }} sm={{ span: 12 }} xs={{ span: 24 }}>
            <Form.Item
              name="districtName"
              label="Quận/huyện"
              rules={[{ required: true, message: "Vui lòng chọn quận/huyện" }]}
            >
              <Select
                placeholder="Chọn quận/huyện"
                showSearch
                allowClear
                options={districtList}
              />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <Button htmlType="submit" block type="primary" loading={loading}>
            Hoàn tất
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default SignUpForm;
