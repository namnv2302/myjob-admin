import { Col, Row, Typography } from "antd";
import SidebarLeft from "@pages/Chats/components/SidebarLeft";

const ChatsPage = () => {
  return (
    <>
      <Typography.Title level={5}>Tin nhắn của ứng viên</Typography.Title>
      <Row>
        <Col lg={{ span: 6 }}>
          <SidebarLeft />
        </Col>
        <Col lg={{ span: 18 }}></Col>
      </Row>
    </>
  );
};

export default ChatsPage;
