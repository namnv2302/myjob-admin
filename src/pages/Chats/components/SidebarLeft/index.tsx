import { memo } from "react";
import { Divider, Input } from "antd";
import { FilterOutlined, SearchOutlined } from "@ant-design/icons";
import classNames from "classnames/bind";
import styles from "./SidebarLeft.module.scss";
import ChatItem from "@components/ChatItem";
import { useAppSelector } from "redux/hooks";
import useChats from "@hooks/chats/useChats";

const cx = classNames.bind(styles);

const SidebarLeft = () => {
  const authorization = useAppSelector((state) => state.authorization);
  const { data, loading } = useChats(`${authorization?.company?.id}`);
  console.log(data);

  return (
    <div className={cx("wrapper")}>
      <div className={cx("actions")}>
        <Input prefix={<SearchOutlined />} placeholder="Tìm kiếm" />
        <FilterOutlined />
      </div>
      <Divider />
      <div className="body">
        <ChatItem />
      </div>
    </div>
  );
};

export default memo(SidebarLeft);
