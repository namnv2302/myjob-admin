import { Role } from "@constants/common";
import { useAppSelector } from "redux/hooks";
import CVTable from "@pages/CV/components/CVTable";

const CVPage = () => {
  const authorization = useAppSelector((state) => state.authorization);

  return (
    <>
      {authorization?.role === Role[2].key ? (
        <CVTable />
      ) : (
        <CVTable companyId={authorization?.company?.id} />
      )}
    </>
  );
};

export default CVPage;
