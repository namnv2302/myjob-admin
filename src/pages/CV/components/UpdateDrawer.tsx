import { memo, useCallback, useEffect, useState } from "react";
import {
  Button,
  Col,
  Drawer,
  Form,
  Input,
  Row,
  Select,
  Space,
  message,
} from "antd";
import { IResumes } from "@slices/authorization/authorizationSlice";
import { Status } from "@constants/common";
import { updateResumes } from "@apis/resumes";

const UpdateDrawer = ({
  data,
  open,
  onOpen,
}: {
  data: IResumes | null;
  open: boolean;
  onOpen: any;
}) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    form.setFieldsValue({
      email: data?.email,
      fullname: data?.fullname,
      status: data?.status,
      job: data?.job?.name,
    });
  }, [data, form]);

  const handleFinish = useCallback(
    async (formData: any) => {
      setLoading(true);
      try {
        const resp = await updateResumes(`${data?.id}`, {
          status: formData.status,
        });
        if (resp.status === 200) {
          message.success("Cập nhật thành công, tải lại");
          form.resetFields();
          onOpen(false);
        }
      } catch (error) {
        message.error("Có lỗi, thử lại");
      }
      setLoading(false);
    },
    [data?.id, form, onOpen]
  );

  return (
    <Drawer
      title="Sửa trạng thái"
      placement="right"
      width={500}
      forceRender
      open={open}
      onClose={() => {
        onOpen(false);
        form.resetFields();
      }}
      extra={
        <Space>
          <Button
            type="primary"
            onClick={() => form.submit()}
            loading={loading}
          >
            Sửa
          </Button>
        </Space>
      }
    >
      <Form
        form={form}
        name="update-cv"
        layout="vertical"
        requiredMark="optional"
        initialValues={{
          email: data?.email,
          fullname: data?.fullname,
          status: data?.status,
          job: data?.job?.name,
        }}
        onFinish={handleFinish}
      >
        <Row gutter={{ lg: 16 }}>
          <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item name="email" label="Email" required>
              <Input placeholder="Email" disabled />
            </Form.Item>
          </Col>
          <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item name="fullname" label="Họ tên" required>
              <Input placeholder="Họ tên" disabled />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={{ lg: 16 }}>
          <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item name="job" label="Việc làm ứng tuyển" required>
              <Input placeholder="Việc làm ứng tuyển" disabled />
            </Form.Item>
          </Col>
          <Col lg={{ span: 12 }} sm={{ span: 24 }} xs={{ span: 24 }}>
            <Form.Item name="status" label="Trạng thái" required>
              <Select
                placeholder="Trạng thái"
                options={Status.map(({ key, label }) => ({
                  key,
                  value: key,
                  label,
                }))}
              />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};

export default memo(UpdateDrawer);
