import { memo, useCallback, useEffect, useMemo, useState } from "react";
import dayjs from "dayjs";
import { Button, Image, Pagination, Tag, message } from "antd";
import {
  CheckCircleOutlined,
  ClockCircleOutlined,
  DislikeOutlined,
  EditOutlined,
  EyeOutlined,
  MailOutlined,
  ReloadOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { ProTable } from "@ant-design/pro-components";
import classNames from "classnames/bind";
import styles from "@pages/CV/CV.module.scss";
import useResumes from "@hooks/resumes/useResumes";
import { DEFAULT_TIME_FORMAT } from "@constants/time";
import { Status } from "@constants/common";
import { IJobs, IResumes } from "@slices/authorization/authorizationSlice";
import { getResumesList } from "@apis/resumes";
import UpdateDrawer from "@pages/CV/components/UpdateDrawer";
import images from "@assets/images";

const cx = classNames.bind(styles);

const CVTable = ({ companyId }: { companyId?: number }) => {
  const { data, loading, totalPages, currentPage } = useResumes(1, companyId);
  const [current, setCurrent] = useState<number>();
  const [resumesData, setResumesData] = useState<IResumes[]>();
  const [fetching, setFetching] = useState<boolean>(false);
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);
  const [resumesItem, setResumesItem] = useState<IResumes | null>(null);

  useEffect(() => {
    if (data) {
      setResumesData(data);
      setCurrent(currentPage);
    }
  }, [data, currentPage]);

  const handleChangePage = useCallback(
    async (page: number) => {
      setFetching(true);
      try {
        const resp = await getResumesList(page, companyId);
        if (resp.status === 200) {
          setResumesData(resp.data.data);
          setCurrent(resp.data.meta.current);
        }
      } catch (error) {
        message.error("Có lỗi, thử lại sau");
      }
      setFetching(false);
    },
    [companyId]
  );

  const handleReload = useCallback(async () => {
    setFetching(true);
    try {
      const resp = await getResumesList(1, companyId);
      if (resp.status === 200) {
        setResumesData(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, [companyId]);

  const handleBeforeOpenDrawer = useCallback((record: IResumes) => {
    setResumesItem(record);
    setOpenDrawer(true);
  }, []);

  const columns: any = useMemo(
    () => [
      {
        title: "Ứng viên",
        key: "fullname",
        render: (record: IResumes) => (
          <div className={cx("sendBy")}>
            <Image
              src={record.sendBy?.avatar || images.avatarDefault}
              alt={record.sendBy?.fullname}
              preview={false}
              width={50}
              height={50}
              style={{ borderRadius: "100%" }}
            />
            <div className={cx("info")}>
              <h5 className="line-clamp-1">{record.sendBy?.fullname}</h5>
              {record.sendBy?.isVerify ? (
                <Tag color="success" className={cx("tag")}>
                  Đã xác thực
                </Tag>
              ) : (
                <Tag color="error" className={cx("tag")}>
                  Chưa xác thực
                </Tag>
              )}
            </div>
          </div>
        ),
      },
      {
        title: "Việc làm ứng tuyển",
        key: "job",
        dataIndex: "job",
        width: "32%",
        render: (job: IJobs) => {
          return (
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "start",
                gap: "4px",
              }}
            >
              <p className={cx("job-name")}>{job.name}</p>
              <Tag className={cx("tag")}>{`#${job.id}`}</Tag>
            </div>
          );
        },
      },
      {
        title: "Email",
        key: "email",
        dataIndex: "email",
        render: (email: string) => (
          <p className={cx("email")}>
            <MailOutlined style={{ color: "#007456" }} />
            <span>{email}</span>
          </p>
        ),
      },
      {
        title: "Ngày nộp",
        key: "createdAt",
        dataIndex: "createdAt",
        render: (value: any) => {
          return (
            <p className={cx("email")}>
              <ClockCircleOutlined />
              <span>{dayjs(value).format(DEFAULT_TIME_FORMAT)}</span>
            </p>
          );
        },
      },
      {
        title: "Trạng thái",
        key: "status",
        dataIndex: "status",
        render: (value: number) => {
          if (Status[0].key === value) {
            return (
              <Tag
                icon={<SyncOutlined />}
                color="processing"
                className={cx("tag")}
              >
                Chờ xử lý
              </Tag>
            );
          } else if (Status[1].key === value) {
            return (
              <Tag
                icon={<CheckCircleOutlined />}
                color="success"
                className={cx("tag")}
              >
                Đồng ý
              </Tag>
            );
          }
          return (
            <Tag icon={<DislikeOutlined />} color="error" className={cx("tag")}>
              Từ chối
            </Tag>
          );
        },
      },
      {
        title: "Cập nhật",
        key: "action",
        align: "center" as "center",
        width: "10%",
        render: (_: any, record: IResumes) => (
          <div className={cx("action")}>
            <Button
              type="link"
              href={record.fileUrl}
              target="_blank"
              style={{ color: "magenta" }}
              icon={<EyeOutlined />}
            ></Button>
            <Button
              ghost
              icon={<EditOutlined />}
              style={{ color: "green" }}
              className={cx("btn-delete")}
              onClick={() => handleBeforeOpenDrawer(record)}
            ></Button>
          </div>
        ),
      },
    ],
    [handleBeforeOpenDrawer]
  );

  return (
    <>
      <ProTable
        headerTitle="Hồ sơ đã nhận"
        rowKey="id"
        // bordered
        cardBordered
        search={false}
        options={false}
        pagination={false}
        loading={loading || fetching}
        columns={columns}
        dataSource={resumesData}
        toolBarRender={() => [
          <Button
            key="button-reload"
            icon={<ReloadOutlined />}
            onClick={handleReload}
          >
            Tải lại
          </Button>,
        ]}
      />
      <Pagination
        style={{ marginTop: 26, textAlign: "center" }}
        current={current}
        pageSize={10}
        total={10 * totalPages}
        onChange={handleChangePage}
      />

      <UpdateDrawer
        data={resumesItem}
        open={openDrawer}
        onOpen={setOpenDrawer}
      />
    </>
  );
};

export default memo(CVTable);
