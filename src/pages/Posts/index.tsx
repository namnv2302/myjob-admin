import NotPermission from "@components/NotPermission";
import { Role } from "@constants/common";
import { useAppSelector } from "redux/hooks";
import PostsTable from "@pages/Posts/components/PostsTable";

const PostsPage = () => {
  const authorization = useAppSelector((state) => state.authorization);

  return (
    <>
      {authorization?.role === Role[2].key ? (
        <PostsTable />
      ) : (
        <NotPermission
          status="403"
          title="403"
          subTitle="Bạn không được phép truy cập trang này"
        />
      )}
    </>
  );
};

export default PostsPage;
