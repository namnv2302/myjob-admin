import { memo, useCallback, useEffect, useMemo, useState } from "react";
import { Button, Pagination, Popconfirm, message } from "antd";
import dayjs from "dayjs";
import { ProTable } from "@ant-design/pro-components";
import {
  ClockCircleOutlined,
  DeleteOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import {
  AuthorizationData,
  IPost,
} from "@slices/authorization/authorizationSlice";
import usePosts from "@hooks/posts/usePosts";
import { DEFAULT_TIME_FORMAT } from "@constants/time";
import { deletePost, getPostList } from "@apis/posts";

const PostsTable = () => {
  const { data, loading, currentPage, totalPages } = usePosts(1, 10);
  const [current, setCurrent] = useState<number>();
  const [postsData, setPostsData] = useState<IPost[]>();
  const [fetching, setFetching] = useState<boolean>(false);

  useEffect(() => {
    if (data) {
      setPostsData(data);
      setCurrent(currentPage);
    }
  }, [data, currentPage]);

  const handleDeletePost = useCallback(async (id: string) => {
    try {
      await deletePost(id);
      message.success("Xóa thành công, tải lại");
    } catch (error) {
      message.error("Thử lại sau");
    }
  }, []);

  const handleChangePage = useCallback(async (page: number) => {
    setFetching(true);
    try {
      const resp = await getPostList(page);
      if (resp.status === 200) {
        setPostsData(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, []);

  const handleReload = useCallback(async () => {
    setFetching(true);
    try {
      const resp = await getPostList(1);
      if (resp.status === 200) {
        setPostsData(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, []);

  const columns: any = useMemo(
    () => [
      {
        title: "Tiêu đề",
        key: "title",
        dataIndex: "title",
        width: "40%",
        render: (title: string) => <p className="line-clamp-1">{title}</p>,
      },
      {
        title: "Người viết",
        key: "owner",
        dataIndex: "owner",
        render: (owner: AuthorizationData) => {
          return owner.fullname || "User deleted";
        },
      },
      {
        title: "Ngày tạo",
        key: "createdAt",
        dataIndex: "createdAt",
        render: (value: any) => (
          <p style={{ display: "flex", gap: "8px" }}>
            <ClockCircleOutlined />
            <span>{dayjs(value).format(DEFAULT_TIME_FORMAT)}</span>
          </p>
        ),
      },
      {
        title: "Cập nhật",
        key: "action",
        align: "center" as "center",
        render: (_: any, record: any) => (
          <>
            <Popconfirm
              title="Xóa bài viết"
              description="Bạn có chắc chắn không?"
              okText="Xóa"
              cancelText="Hủy"
              onConfirm={() => handleDeletePost(`${record.id}`)}
            >
              <Button danger icon={<DeleteOutlined color="red" />}></Button>
            </Popconfirm>
          </>
        ),
      },
    ],
    [handleDeletePost]
  );

  return (
    <>
      <ProTable
        headerTitle="Danh sách bài viết"
        rowKey="id"
        bordered
        cardBordered
        search={false}
        options={false}
        pagination={false}
        loading={loading || fetching}
        columns={columns}
        dataSource={postsData}
        toolBarRender={() => [
          <>
            <Button
              key="button-reload"
              icon={<ReloadOutlined />}
              onClick={handleReload}
            >
              Tải lại
            </Button>
          </>,
        ]}
      />
      <Pagination
        style={{ marginTop: 26, textAlign: "center" }}
        current={current}
        pageSize={10}
        total={10 * totalPages}
        onChange={handleChangePage}
      />
    </>
  );
};

export default memo(PostsTable);
