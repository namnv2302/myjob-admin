import { memo, useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  ClockCircleOutlined,
  DeleteOutlined,
  DollarOutlined,
  EditOutlined,
  PlusOutlined,
  ReloadOutlined,
} from "@ant-design/icons";
import dayjs from "dayjs";
import { ProTable } from "@ant-design/pro-components";
import { Button, Pagination, Popconfirm, Tag, message } from "antd";
import classNames from "classnames/bind";
import styles from "@pages/Jobs/Jobs.module.scss";
import { ROUTE_PATH } from "@constants/routes";
import useJobs from "@hooks/jobs/useJobs";
import { DEFAULT_TIME_FORMAT } from "@constants/time";
import { Level } from "@constants/common";
import { deleteJob, getJobsList } from "@apis/jobs";
import { IJobs } from "@slices/authorization/authorizationSlice";

const cx = classNames.bind(styles);

const JobsTable = ({ companyId }: { companyId?: number }) => {
  const navigate = useNavigate();
  const { data, loading, currentPage, totalPages } = useJobs(1, companyId);
  const [current, setCurrent] = useState<number>();
  const [jobsData, setJobsData] = useState<IJobs[]>();
  const [fetching, setFetching] = useState<boolean>(false);

  useEffect(() => {
    if (data) {
      setJobsData(data);
      setCurrent(currentPage);
    }
  }, [data, currentPage]);

  const handleDeleteJob = useCallback(async (id: string) => {
    try {
      await deleteJob(id);
      message.success("Xóa thành công, tải lại");
    } catch (error) {
      message.error("Thử lại sau");
    }
  }, []);

  const handleChangePage = useCallback(
    async (page: number) => {
      setFetching(true);
      try {
        const resp = await getJobsList(page, companyId);
        if (resp.status === 200) {
          setJobsData(resp.data.data);
          setCurrent(resp.data.meta.current);
        }
      } catch (error) {
        message.error("Có lỗi, thử lại sau");
      }
      setFetching(false);
    },
    [companyId]
  );

  const handleReload = useCallback(async () => {
    setFetching(true);
    try {
      const resp = await getJobsList(1, companyId);
      if (resp.status === 200) {
        setJobsData(resp.data.data);
        setCurrent(resp.data.meta.current);
      }
    } catch (error) {
      message.error("Có lỗi, thử lại sau");
    }
    setFetching(false);
  }, [companyId]);

  const columns: any = useMemo(
    () => [
      {
        title: "Tên việc làm",
        key: "name",
        dataIndex: "name",
        width: "32%",
        render: (name: string) => <p className={cx("job-name")}>{name}</p>,
      },
      {
        title: "Mức lương",
        key: "salary",
        dataIndex: "salary",
        render: (salary: string) => (
          <p className={cx("salary")}>
            <DollarOutlined />
            <span>{salary}</span>
          </p>
        ),
      },
      {
        title: "Trình độ",
        key: "level",
        dataIndex: "level",
        render: (value: any) => (
          <Tag>{Level.find(({ key }) => key === value)?.label}</Tag>
        ),
      },
      {
        title: "Ngày bắt đầu",
        key: "startDate",
        dataIndex: "startDate",
        render: (value: any) => (
          <p className={cx("salary")}>
            <ClockCircleOutlined />
            <span>{dayjs(value).format(DEFAULT_TIME_FORMAT)}</span>
          </p>
        ),
      },
      {
        title: "Hết hạn",
        key: "endDate",
        dataIndex: "endDate",
        render: (value: any) => (
          <p className={cx("salary")}>
            <ClockCircleOutlined />
            <span>{dayjs(value).format(DEFAULT_TIME_FORMAT)}</span>
          </p>
        ),
      },
      {
        title: "Cập nhật",
        key: "action",
        align: "center" as "center",
        render: (_: any, record: any) => (
          <>
            <Button
              type="primary"
              ghost
              icon={<EditOutlined />}
              onClick={() =>
                navigate(`${ROUTE_PATH.JOBS_DETAIL.replace(":id", record.id)}`)
              }
            ></Button>
            <Popconfirm
              title="Xóa việc làm"
              description="Bạn có chắc chắn không?"
              okText="Xóa"
              cancelText="Hủy"
              onConfirm={() => handleDeleteJob(`${record.id}`)}
            >
              <Button
                danger
                icon={<DeleteOutlined color="red" />}
                className={cx("btn-delete")}
              ></Button>
            </Popconfirm>
          </>
        ),
      },
    ],
    [navigate, handleDeleteJob]
  );

  return (
    <>
      <ProTable
        headerTitle="Danh sách việc làm"
        rowKey="id"
        cardBordered
        search={false}
        options={false}
        pagination={false}
        loading={loading || fetching}
        dataSource={jobsData}
        columns={columns}
        toolBarRender={() => [
          <>
            <Button
              key="button-add"
              type="primary"
              icon={<PlusOutlined />}
              onClick={() => navigate(`${ROUTE_PATH.JOBS_CREATE}`)}
            >
              Thêm
            </Button>
            <Button
              key="button-reload"
              icon={<ReloadOutlined />}
              onClick={handleReload}
            >
              Tải lại
            </Button>
          </>,
        ]}
      />
      <Pagination
        style={{ marginTop: 26, textAlign: "center" }}
        current={current}
        pageSize={10}
        total={10 * totalPages}
        onChange={handleChangePage}
      />
    </>
  );
};

export default memo(JobsTable);
