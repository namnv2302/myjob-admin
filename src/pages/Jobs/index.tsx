import { Role } from "@constants/common";
import JobsTable from "@pages/Jobs/components/JobsTable";
import { useAppSelector } from "redux/hooks";

const JobsPage = () => {
  const authorization = useAppSelector((state) => state.authorization);

  return (
    <>
      {authorization?.role === Role[2].key ? (
        <JobsTable />
      ) : (
        <JobsTable companyId={authorization?.company?.id} />
      )}
    </>
  );
};

export default JobsPage;
