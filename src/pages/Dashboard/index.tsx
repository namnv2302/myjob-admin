import { useEffect, useState } from "react";
import { Card, Col, Row, Statistic } from "antd";
import { useAppSelector } from "redux/hooks";
import { getInfoAll } from "@apis/resumes";
import { Role } from "@constants/common";
import axiosInstance from "@utils/axios";

const DashboardPage = () => {
  const authorization = useAppSelector((state) => state.authorization);
  const [result, setResult] = useState({
    status0: 0,
    status1: 0,
    status2: 0,
    jobsCount: 0,
    companiesCount: 0,
    usersCount: 0,
    employersCount: 0,
  });

  useEffect(() => {
    (async () => {
      if (authorization?.role === Role[1].key && authorization?.company?.id) {
        const result = (await getInfoAll(authorization?.company?.id)).data;
        setResult(result);
      }
      if (authorization?.role === Role[2].key) {
        const result = (await axiosInstance.get("")).data;
        setResult(result);
      }
    })();
  }, [authorization]);

  return (
    <Row gutter={12}>
      {authorization?.role === Role[1].key ? (
        <>
          <Col lg={{ span: 6 }} xs={{ span: 24 }} sm={{ span: 24 }}>
            <Card bordered={false}>
              <Statistic
                title="Tổng CV đã nhận"
                value={result.status0 + result.status1 + result.status2}
                valueStyle={{ fontSize: "18px" }}
              />
            </Card>
          </Col>
          <Col lg={{ span: 6 }} xs={{ span: 24 }} sm={{ span: 24 }}>
            <Card bordered={false}>
              <Statistic
                title="CV chờ xử lý"
                value={result.status0}
                valueStyle={{ color: "magenta", fontSize: "18px" }}
              />
            </Card>
          </Col>
          <Col lg={{ span: 6 }} xs={{ span: 24 }} sm={{ span: 24 }}>
            <Card bordered={false}>
              <Statistic
                title="CV đã đồng ý"
                value={result.status1}
                valueStyle={{ color: "#3f8600", fontSize: "18px" }}
              />
            </Card>
          </Col>
          <Col lg={{ span: 6 }} xs={{ span: 24 }} sm={{ span: 24 }}>
            <Card bordered={false}>
              <Statistic
                title="CV đã từ chối"
                value={result.status2}
                valueStyle={{ color: "#cf1322", fontSize: "18px" }}
              />
            </Card>
          </Col>
        </>
      ) : (
        <>
          <Col lg={{ span: 6 }} xs={{ span: 24 }} sm={{ span: 24 }}>
            <Card bordered={false}>
              <Statistic
                title="Số công ty"
                value={result.companiesCount}
                valueStyle={{ fontSize: "18px" }}
              />
            </Card>
          </Col>
          <Col lg={{ span: 6 }} xs={{ span: 24 }} sm={{ span: 24 }}>
            <Card bordered={false}>
              <Statistic
                title="Số việc làm"
                value={result.jobsCount}
                valueStyle={{ color: "magenta", fontSize: "18px" }}
              />
            </Card>
          </Col>
          <Col lg={{ span: 6 }} xs={{ span: 24 }} sm={{ span: 24 }}>
            <Card bordered={false}>
              <Statistic
                title="Số nhà tuyển dụng"
                value={result.employersCount}
                valueStyle={{ color: "#3f8600", fontSize: "18px" }}
              />
            </Card>
          </Col>
          <Col lg={{ span: 6 }} xs={{ span: 24 }} sm={{ span: 24 }}>
            <Card bordered={false}>
              <Statistic
                title="Số ứng viên"
                value={result.usersCount}
                valueStyle={{ color: "#cf1322", fontSize: "18px" }}
              />
            </Card>
          </Col>
        </>
      )}
    </Row>
  );
};

export default DashboardPage;
