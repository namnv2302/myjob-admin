import { useMemo } from "react";
import { useParams } from "react-router-dom";
import useJobDetail from "@hooks/jobs/useJobDetail";
import UpdateCard from "@pages/JobsDetail/components/UpdateCard";

const JobsDetailPage = () => {
  const { id } = useParams();
  const jobId = useMemo(() => `${id}`, [id]);
  const { data, loading } = useJobDetail(jobId);

  return (
    <>
      <UpdateCard job={data} fetching={loading} />
    </>
  );
};

export default JobsDetailPage;
