import { memo, useCallback, useEffect, useRef, useState } from "react";
import axios from "axios";
import dayjs from "dayjs";
import { useNavigate } from "react-router-dom";
import {
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  Row,
  Select,
  Space,
  Switch,
  message,
} from "antd";
import type { InputRef, DatePickerProps } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import ReactQuill from "react-quill";
import { Level, Skills } from "@constants/common";
import { ProvinceType } from "@pages/SignUp/components/SignUpForm";
import { IJobs } from "@slices/authorization/authorizationSlice";
import { ROUTE_PATH } from "@constants/routes";
import { FormDataJobType } from "@pages/JobsCreate/components/CreateCard";
import { updateJob } from "@apis/jobs";

const UpdateCard = ({
  job,
  fetching,
}: {
  job: IJobs | undefined;
  fetching: boolean;
}) => {
  const [form] = Form.useForm();
  const navigate = useNavigate();
  const inputRef = useRef<InputRef>(null);
  const [provinceList, setProvinceList] = useState<ProvinceType[]>([]);
  const [description, setDescription] = useState<string>("");
  const [isActive, setIsActive] = useState<boolean>(false);
  const [creating, setCreating] = useState<boolean>(false);
  const [items, setItems] = useState(Skills);
  const [skill, setSkill] = useState("");

  useEffect(() => {
    form.setFieldsValue({
      name: job?.name,
      skills: job?.skills,
      location: job?.location,
      salary: job?.salary,
      quantity: job?.quantity,
      level: job?.level,
      startDate: dayjs(job?.startDate),
      endDate: dayjs(job?.endDate),
    });
  }, [form, job]);

  useEffect(() => {
    if (job?.description) {
      setDescription(job.description);
    } else {
      setDescription("");
    }
    if (job?.isActive) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [job?.description, job?.isActive]);

  // Get province list
  useEffect(() => {
    (async () => {
      const resp = await axios.get(
        `${process.env.REACT_APP_PROVINCE_BASE_URL}/province`
      );
      if (resp.status === 200) {
        const list = resp.data.results.map((item: ProvinceType) => ({
          key: item.province_id,
          value: item.province_name,
          label: item.province_name,
        }));
        setProvinceList(list);
      }
    })();
  }, []);

  const onSkillChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setSkill(event.target.value);
    },
    []
  );

  const addSkill = useCallback(
    (e: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => {
      e.preventDefault();
      setItems([...items, skill]);
      setSkill("");
      setTimeout(() => {
        inputRef.current?.focus();
      }, 0);
    },
    [items, skill]
  );

  const disabledDate: DatePickerProps["disabledDate"] = useCallback(
    (current: any) => {
      return current && current.isBefore(dayjs());
    },
    []
  );

  const handleFinish = useCallback(
    async (formData: FormDataJobType) => {
      setCreating(true);
      try {
        const resp = await updateJob(`${job?.id}`, {
          ...formData,
          isActive: isActive,
          description: description,
        });
        if (resp.status === 200) {
          message.success("Chỉnh sửa thành công");
          navigate(ROUTE_PATH.JOBS);
          form.resetFields();
          setDescription("");
          setIsActive(true);
        }
      } catch (error) {
        if (axios.isAxiosError(error)) {
          message.error(error.response?.data.message);
        } else {
          message.error("Có lỗi. Thử lại sau!");
        }
      }
      setCreating(false);
    },
    [description, navigate, form, isActive, job?.id]
  );

  return (
    <Row>
      <Col lg={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
        <Card
          title="Chỉnh sửa việc làm"
          headStyle={{ borderBottom: 0 }}
          loading={fetching}
        >
          <Form
            name="create-job"
            form={form}
            layout="vertical"
            onFinish={handleFinish}
            initialValues={{
              name: job?.name,
              skills: job?.skills,
              location: job?.location,
              salary: job?.salary,
              quantity: job?.quantity,
              level: job?.level,
              startDate: dayjs(job?.startDate),
              endDate: dayjs(job?.endDate),
            }}
          >
            <Row gutter={{ lg: 16 }}>
              <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 24 }}>
                <Form.Item
                  name="name"
                  label="Tên việc làm"
                  rules={[
                    { required: true, message: "Tên không được để trống" },
                  ]}
                >
                  <Input placeholder="Tên việc làm" />
                </Form.Item>
              </Col>
              <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 24 }}>
                <Row gutter={{ lg: 16, sm: 12, xs: 12 }}>
                  <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 12 }}>
                    <Form.Item
                      name="skills"
                      label="Kỹ năng yêu cầu"
                      rules={[
                        {
                          required: true,
                          message: "Kỹ năng không được để trống",
                        },
                      ]}
                    >
                      <Select
                        mode="multiple"
                        allowClear
                        style={{ width: "100%" }}
                        placeholder="Chọn kỹ năng"
                        dropdownRender={(menu) => (
                          <>
                            {menu}
                            <Divider style={{ margin: "8px 0" }} />
                            <Space style={{ padding: "0 8px 4px" }}>
                              <Input
                                placeholder="Thêm kỹ năng"
                                ref={inputRef}
                                value={skill}
                                onChange={onSkillChange}
                                onKeyDown={(e) => e.stopPropagation()}
                              />
                              <Button
                                type="text"
                                icon={<PlusOutlined />}
                                onClick={addSkill}
                              >
                                Thêm
                              </Button>
                            </Space>
                          </>
                        )}
                        options={items.map((item) => ({
                          key: item,
                          value: item,
                          label: item,
                        }))}
                      />
                    </Form.Item>
                  </Col>
                  <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 12 }}>
                    <Form.Item
                      name="location"
                      label="Địa điểm làm việc"
                      rules={[
                        {
                          required: true,
                          message: "Vui lòng chọn địa điểm làm việc",
                        },
                      ]}
                    >
                      <Select
                        placeholder="Chọn tỉnh/thành phố"
                        showSearch
                        allowClear
                        options={provinceList}
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row gutter={{ lg: 16 }}>
              <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 24 }}>
                <Row gutter={{ lg: 16, xs: 12, sm: 12 }}>
                  <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 12 }}>
                    <Form.Item
                      name="salary"
                      label="Mức lương"
                      rules={[
                        {
                          required: true,
                          message: "Mức lương không được để trống",
                        },
                      ]}
                    >
                      <Input placeholder="Mức lương" />
                    </Form.Item>
                  </Col>
                  <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 12 }}>
                    <Form.Item
                      name="quantity"
                      label="Số lượng tuyển dụng"
                      rules={[
                        {
                          required: true,
                          message: "Số lượng không được để trống",
                        },
                      ]}
                    >
                      <Input placeholder="Số lượng" />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 24 }}>
                <Row gutter={{ lg: 16, xs: 12, sm: 12 }}>
                  <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 12 }}>
                    <Form.Item
                      name="level"
                      label="Trình độ"
                      rules={[
                        {
                          required: true,
                          message: "Trình độ không được để trống",
                        },
                      ]}
                    >
                      <Select
                        allowClear
                        placeholder="Chọn trình độ"
                        options={Level.map((item) => ({
                          key: item.key,
                          value: item.key,
                          label: item.label,
                        }))}
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row gutter={{ lg: 16, xs: 12, sm: 12 }}>
              <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 24 }}>
                <Row gutter={{ lg: 16, xs: 12, sm: 12 }}>
                  <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 12 }}>
                    <Form.Item
                      name="startDate"
                      label="Ngày bắt đầu"
                      rules={[
                        { required: true, message: "Không được để trống" },
                      ]}
                    >
                      <DatePicker disabledDate={disabledDate} />
                    </Form.Item>
                  </Col>
                  <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 12 }}>
                    <Form.Item
                      name="endDate"
                      label="Hết hạn"
                      rules={[
                        { required: true, message: "Không được để trống" },
                      ]}
                    >
                      <DatePicker disabledDate={disabledDate} />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col lg={{ span: 12 }} xs={{ span: 24 }} sm={{ span: 24 }}>
                <Form.Item label="Trạng thái">
                  <Switch
                    checked={isActive}
                    checkedChildren="Hoạt động"
                    unCheckedChildren="Hết hạn"
                    onChange={setIsActive}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col lg={{ span: 24 }} xs={{ span: 24 }} sm={{ span: 24 }}>
                <Form.Item label="Miêu tả công việc" required>
                  <ReactQuill
                    theme="snow"
                    placeholder="Miêu tả công việc"
                    value={description}
                    onChange={setDescription}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col lg={{ span: 24 }} xs={{ span: 24 }} sm={{ span: 24 }}>
                <Form.Item style={{ marginBottom: 0 }}>
                  <Button
                    type="primary"
                    block
                    htmlType="submit"
                    loading={creating}
                  >
                    Chỉnh sửa
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
      </Col>
    </Row>
  );
};

export default memo(UpdateCard);
