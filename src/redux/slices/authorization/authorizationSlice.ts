import { createSlice } from "@reduxjs/toolkit";

export const AdminRole = ["admin", "employer"];

export interface IChats {
  id?: number;
  firstId?: string;
  secondId?: string;
  isDeleted?: boolean;
  sendLastAt?: Date;
}

export interface IResumes {
  id?: number;
  fullname?: string;
  email?: string;
  fileUrl?: string;
  status?: boolean;
  job?: IJobs;
  sendBy?: AuthorizationData;
}

export interface IJobs {
  id?: number;
  name?: string;
  description?: string;
  skills?: string[];
  location?: string;
  salary?: string;
  level?: string;
  quantity?: string;
  startDate?: Date;
  endDate?: Date;
  company?: ICompanies;
  isActive?: boolean;
}

export interface IPost {
  id?: number;
  title?: string;
  content?: string;
  owner?: AuthorizationData | number;
  createdAt?: Date;
  bookmarkList?: AuthorizationData[];
}

export interface ICompanies {
  id?: number;
  companyName?: string;
  provinceName?: string;
  districtName?: string;
  introduction?: string;
  logo?: string;
  scales?: string;
}

export interface AuthorizationData {
  id?: number;
  fullname?: string;
  email?: string;
  gender?: "male" | "female";
  phoneNumber?: number;
  avatar?: string;
  role?: string;
  isVerify?: boolean;
  company?: ICompanies;
}

type AuthorizationState = AuthorizationData | null;

const initialState = null as AuthorizationState;

const authorizationSlice = createSlice({
  name: "authorization",
  initialState,
  reducers: {
    logout: () => null,
    login: (state, action) => {
      return action.payload;
    },
    updateProfile: (state, action) => {
      return action.payload;
    },
    updateAuthorizationCompany: (state, action) => {
      if (state) {
        state.company = action.payload;
      }
    },
  },
});

export const { login, logout, updateAuthorizationCompany, updateProfile } =
  authorizationSlice.actions;
export default authorizationSlice.reducer;
