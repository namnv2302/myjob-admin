# stage 1
# FROM node:14-alpine as build-stage

# WORKDIR /usr/myjob-client
# COPY myjob-client/ .

# RUN npm install

# RUN npm run build

# stage 2
FROM nginx:1.23-alpine

# COPY --from=build-stage /usr/myjob-client/build /usr/share/nginx/html

# CMD nginx -g 'daemon off;'